const express = require("express");
const router = express.Router();
const db = require("../db");

router.get("/id/:id", async (req, res) => {
  console.log("id:");
  var id = req.params.id;
  console.log(id);

  const sqlCompetitions =
    `SELECT DISTINCT idnatjecanje, nazivnat, TO_CHAR(datumnat, 'DD.MM.YYYY') AS datumtnat, nazivmjesto, nazivdrzava, ime, prezime, plasman, nazivstil
  FROM natjecanje NATURAL JOIN se_natjecao_na NATURAL JOIN mjesto NATURAL JOIN drzava NATURAL JOIN clankluba NATURAL JOIN korisnik
  NATURAL JOIN plesnistil WHERE idnatjecanje = ` + id;
  try {
    const competitions = (await db.query(sqlCompetitions, [])).rows;
    res.json(competitions);
  } catch (err) {
    console.log(err);
  }
});

router.get("/", async (req, res) => {
  const sqlCompetitions = `SELECT DISTINCT idnatjecanje, nazivnat, TO_CHAR(datumnat, 'DD.MM.YYYY') AS datumnat, nazivmjesto, nazivdrzava FROM 
  natjecanje NATURAL JOIN se_natjecao_na NATURAL JOIN mjesto NATURAL JOIN drzava`;
  try {
    const competitions = (await db.query(sqlCompetitions, [])).rows;
    res.json(competitions);
  } catch (err) {
    console.log(err);
  }
});

router.get("/add", async (req, res) => {
  const sqlCities = `SELECT idmjesto AS value, nazivmjesto AS label FROM mjesto`;
  const sqlMembers = `SELECT idkorisnik, ime || ' '|| prezime AS osoba, isdance AS iscompetitor 
  FROM korisnik NATURAL JOIN clankluba NATURAL JOIN f`;
  try {
    const cities = (await db.query(sqlCities, [])).rows;
    const members = (await db.query(sqlMembers, [])).rows;
    let data = { members: members, cities: cities };
    res.json(data);
  } catch (err) {
    console.log(err);
  }
});

router.post("/add", async (req, res) => {
  const sqlInsert =
    `INSERT INTO natjecanje (nazivnat, datumnat, idmjesto) VALUES ('` +
    req.body.competitionName +
    `','` +
    req.body.competitionDate +
    `', ` +
    req.body.idmjesto +
    `);`;

  try {
    await db.query(sqlInsert);

    const sqlIdNatjecanje =
      `SELECT idNatjecanje FROM natjecanje WHERE nazivNat = '` +
      req.body.competitionName +
      `'`;
    const competition = (await db.query(sqlIdNatjecanje, [])).rows;
    idnatjecanje = competition[0].idnatjecanje;

    for (var i = 0; i < req.body.competitors.length; i++) {
      const sqlMember =
        `SELECT brclanskeiskaznice FROM clankluba WHERE idkorisnik = ` +
        req.body.competitors[i].idkorisnik;
      const member = (await db.query(sqlMember, [])).rows;
      brclanskeiskaznice = member[0].brclanskeiskaznice;

      const sqlInsertSeNatjecaoNa =
        `INSERT INTO se_natjecao_na (brClanskeIskaznice, idStil, idNatjecanje, plasman)
      VALUES ('` +
        brclanskeiskaznice +
        `','` +
        req.body.competitors[i].idstil +
        `',` +
        idnatjecanje +
        `,` +
        req.body.competitors[i].plasman +
        `)`;
      await db.query(sqlInsertSeNatjecaoNa);
    }

    const err = { err: false };
    res.json(err);
  } catch (e) {
    console.log(e);
    const err = { err: true, errmessage: e };
    res.json(err);
  }
});

module.exports = router;
