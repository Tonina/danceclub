const express = require("express");
const router = express.Router();
const db = require("../db");

router.get("/", async (req, res) => {
  console.log(req.params.id);

  const trainingSql = `SELECT DISTINCT  idtrening, TO_CHAR(datumtrening, 'DD.MM.YYYY') AS datumtrening, satPocetak, satKraj, nazivstil, kapacitet, ime, prezime
FROM trening NATURAL JOIN trener NATURAL JOIN dvorana NATURAL JOIN plesnistil
NATURAL JOIN korisnik `;

  try {
    const trainings = (await db.query(trainingSql, [])).rows;
    res.json(trainings);
  } catch (err) {
    console.log(err);
  }
});

router.get("/id/:id", async (req, res) => {
  console.log("id:");
  var id = req.params.id;
  console.log(id);

  const sqlTraining =
    `SELECT DISTINCT ime, prezime, nazivstil, satpocetak,satkraj, kapacitet
  FROM trening NATURAL JOIN dvorana NATURAL JOIN plesnistil NATURAL JOIN sudjeluje_na NATURAL JOIN clankluba
  JOIN korisnik ON clankluba.idkorisnik = korisnik.idkorisnik WHERE idtrening = ` +
    id;
  try {
    const training = (await db.query(sqlTraining, [])).rows;
    res.json(training);
  } catch (err) {
    console.log(err);
  }
});

router.get("/user/:user", async (req, res) => {
  var id = req.params.user;
  console.log(id);
  console.log("U get user");

  const sqlTraining = `SELECT idtrener FROM trening WHERE idtrening =` + id;
  try {
    const training = (await db.query(sqlTraining, [])).rows;
    const idtrener = training[0].idtrener;
    const sqlCoach =
      `SELECT idkorisnik, ime, prezime FROM korisnik NATURAL JOIN trener WHERE idtrener =` +
      idtrener;
    const coach = (await db.query(sqlCoach, [])).rows;

    const sqlCount =
      `SELECT  COUNT(*) FROM trening NATURAL JOIN sudjeluje_na WHERE idtrening =` +
      id +
      `GROUP BY idtrening `;
    const count = (await db.query(sqlCount, [])).rows;

    const data = {
      idkorisnik: coach[0].idkorisnik,
      ime: coach[0].ime,
      prezime: coach[0].prezime,
      count: count[0].count,
    };
    res.json(data);
  } catch (err) {
    console.log(err);
  }
});

router.get("/add", async (req, res) => {
  const sqlDances = `SELECT idstil AS value, nazivstil AS label FROM plesnistil`;
  const sqlMembers = `SELECT idkorisnik, ime || ' '|| prezime AS osoba, isdance AS hasparticipated, idMjesec, idstil
  FROM korisnik NATURAL JOIN clankluba NATURAL JOIN f NATURAL JOIN upisuje`;
  const sqlHalls = `SELECT iddvorana AS value, kapacitet AS label FROM dvorana`;
  try {
    const members = (await db.query(sqlMembers, [])).rows;
    const dances = (await db.query(sqlDances, [])).rows;
    const halls = (await db.query(sqlHalls, [])).rows;
    let data = { members: members, dances: dances, halls: halls };
    res.json(data);
  } catch (err) {
    console.log(err);
  }
});

router.post("/add", async (req, res) => {
  console.log(req.body);

  const selectKorisnik =
    `SELECT idtrener FROM trener WHERE idkorisnik =` + req.body.id;

  try {
    const training = (await db.query(selectKorisnik, [])).rows;
    const idtrener = training[0].idtrener;
    const sqlInsertTraining =
      `INSERT INTO trening (datumTrening, idTrener, idStil, idDvorana, satPocetak, satKraj)
  VALUES ('` +
      req.body.date +
      `',` +
      idtrener +
      `,` +
      req.body.idStyle +
      `,` +
      req.body.idHall +
      `,'` +
      req.body.start +
      `','` +
      req.body.end +
      `' )`;
    await db.query(sqlInsertTraining);

    const sqlIdTraining =
      `SELECT idtrening FROM trening WHERE datumtrening = '` +
      req.body.date +
      `' AND iddvorana=` +
      req.body.idHall;
    const t = (await db.query(sqlIdTraining, [])).rows;
    idtrening = t[0].idtrening;

    for (var i = 0; i < req.body.participants.length; i++) {
      const sqlMember =
        `SELECT brclanskeiskaznice FROM clankluba WHERE idkorisnik = ` +
        req.body.participants[i].idkorisnik;
      const member = (await db.query(sqlMember, [])).rows;
      brclanskeiskaznice = member[0].brclanskeiskaznice;

      const sqlInsertSudjelujeNA =
        `INSERT INTO sudjeluje_na (brClanskeIskaznice, idTrening)
        VALUES ('` +
        brclanskeiskaznice +
        `',` +
        idtrening +
        `)`;
      await db.query(sqlInsertSudjelujeNA);
    }

    const err = { err: false };
    res.json(err);
  } catch (e) {
    console.log(e);
    const err = { err: true };
    res.json(err);
  }
});

module.exports = router;
