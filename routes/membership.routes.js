const express = require("express");
const router = express.Router();
const db = require("../db");

router.get("/id/:id", async (req, res) => {
  console.log("id:");
  var id = req.params.id;
  console.log(id);

  const sqlMembership =
    `SELECT nazivmjesec, idmjesec, cijena, paid
    FROM placa NATURAL JOIN 
    clanarina NATURAL JOIN mjesec NATURAL JOIN clankluba
    NATURAL JOIN korisnik 
    WHERE idkorisnik = ` + id;
  try {
    const memberships = (await db.query(sqlMembership, [])).rows;
    res.json(memberships);
  } catch (err) {
    console.log(err);
  }
});
router.post("/dances", async (req, res) => {
  var d = new Date();
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var year = d.getFullYear();
  let idMjesec = "";
  idMjesec += year;
  if (month < 10) idMjesec += 0;
  idMjesec += month;
  console.log(idMjesec);
  const sqlUser =
    `SELECT brClanskeIskaznice FROM korisnik NATURAL JOIN clankluba WHERE idKorisnik =` +
    req.body.id;

  try {
    const user = (await db.query(sqlUser, [])).rows;
    let brClanskeIskaznice = user[0].brclanskeiskaznice;
    console.log(brClanskeIskaznice);
    const sql =
      `SELECT COUNT(*) FROM upisuje WHERE idmjesec =` +
      idMjesec +
      ` AND brClanskeIskaznice = ` +
      brClanskeIskaznice;
    const count = (await db.query(sql, [])).rows;
    console.log(count);
    if (count[0].count === "0") {
      //nema nista upisano za tekuci mjesec i onda upisujemo u taj mjesec
    } else {
      console.log("u else");
      //upisujemo ga u mjesec nakon
      if (month < 12) {
        month += 1;
      } else {
        year += 1;
        month = 1;
      }
      idMjesec = "";
      idMjesec += year;
      if (month < 10) idMjesec += 0;
      idMjesec += month;
      console.log(idMjesec);
    }

    for (var i = 0; i < req.body.dances.length; i++) {
      console.log(req.body.dances[i]);
      const sqlInsertUpisuje =
        `INSERT INTO upisuje (brclanskeiskaznice, idstil, idmjesec) VALUES (` +
        brClanskeIskaznice +
        `,` +
        req.body.dances[i].idstil +
        `, ` +
        idMjesec +
        `)`;
      await db.query(sqlInsertUpisuje);
    }

    const err = { err: false };
    res.json(err);
  } catch (error) {
    console.log(error);
    const err = { err: true };
    res.json(err);
  }
});
module.exports = router;
