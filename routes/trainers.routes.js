const express = require("express");
const router = express.Router();
const db = require("../db");

const bcrypt = require("bcrypt");
const saltRounds = 10;

router.get("/", async (req, res) => {
  const sqlTrainers = `SELECT ime, prezime, email, TO_CHAR(datumrodenja, 'DD-MM-YYYY') AS datumrodenja,
    brojmobitel, oznakakinfakultet FROM korisnik NATURAL JOIN trener `;
  try {
    const trainers = (await db.query(sqlTrainers, [])).rows;
    res.json(trainers);
  } catch (err) {
    console.log(err);
  }
});

router.post("/add", async function (req, res, next) {
  console.log("u postu");
  console.log(req.body.name);
  const hashedPassword = await bcrypt.hash(req.body.pass, saltRounds);
  const sqlAddUser =
    `INSERT INTO korisnik (ime, prezime, email, lozinka, datumrodenja, OIB, iduloga) VALUES 
    ('` +
    req.body.name +
    `','` +
    req.body.surname +
    `','` +
    req.body.email +
    `','` +
    hashedPassword +
    `','` +
    req.body.datebirth +
    `','` +
    req.body.oib +
    `', '2')`;
  try {
    let idKorisnik = "";
    await db.query(sqlAddUser);
    const sql = `SELECT idKorisnik FROM korisnik WHERE oib = ` + req.body.oib;
    const user = (await db.query(sql, [])).rows;
    idKorisnik = user[0].idkorisnik;
    console.log(idKorisnik);
    const sqlAddTrainer =
      `INSERT INTO trener (oznakaKinFakultet , brojMobitel, idKorisnik)
      VALUES (` +
      req.body.facultydegree +
      `, ` +
      req.body.phone +
      `, ` +
      idKorisnik +
      `)`;
    await db.query(sqlAddTrainer);
    const err = { err: false };
    res.json(err);
  } catch (e) {
    console.log(e);
    const err = { err: true, errmessage: e };
    res.json(err);
  }
});

router.delete("/id/:id", async function (req, res, next) {
  console.log(req.params.id);

  try {
    console.log("U try");
    const sql =
      `SELECT idKorisnik FROM korisnik WHERE email ='` + req.params.id + `'`;
    const trainer = (await db.query(sql, [])).rows;
    console.log(trainer);
    idKorisnik = trainer[0].idkorisnik;
    console.log(idKorisnik);
    const sqlDelete = `DELETE FROM korisnik * WHERE idKorisnik = ` + idKorisnik;
    await db.query(sqlDelete);
    const error = { err: false };
    res.json(error);
  } catch (err) {
    const error = { err: true };
    res.json(error);
  }
});

module.exports = router;
