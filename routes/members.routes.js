const express = require("express");
const router = express.Router();
const db = require("../db");

router.use(express.urlencoded());
router.use(express.json());

const bcrypt = require("bcrypt");
const saltRounds = 10;

router.get("/", async (req, res) => {
  const sqlMembers = `SELECT  ime, prezime, email, TO_CHAR(datumrodenja, 'DD.MM.YYYY') AS datumrodenja,
    TO_CHAR(datumupisa, 'DD.MM.YYYY') AS datumupisa FROM korisnik NATURAL JOIN clankluba WHERE iduloga = 3 `;
  try {
    const members = (await db.query(sqlMembers, [])).rows;
    res.json(members);
  } catch (err) {
    console.log(err);
  }
});

router.post("/add", async function (req, res, next) {
  console.log("u postu");
  console.log(req.body.name);
  var d = new Date();
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var year = d.getFullYear();
  var date = "";
  date += day;
  date += "-";
  if (month < 10) date += 0;
  date += month;
  date += "-";
  date += year;
  console.log(date);

  const hashedPassword = await bcrypt.hash(req.body.pass, saltRounds);

  const sqlAddUser =
    `INSERT INTO korisnik (ime, prezime, email, lozinka, datumrodenja, OIB, iduloga) VALUES 
    ('` +
    req.body.name +
    `','` +
    req.body.surname +
    `','` +
    req.body.email +
    `','` +
    hashedPassword +
    `','` +
    req.body.datebirth +
    `','` +
    req.body.oib +
    `', '3')`;
  try {
    let idMjesec = "";
    idMjesec += year;
    if (month < 10) idMjesec += 0;
    idMjesec += month;
    let idKorisnik = "";

    await db.query(sqlAddUser);
    const sql = `SELECT idKorisnik FROM korisnik WHERE oib = ` + req.body.oib;
    const user = (await db.query(sql, [])).rows;
    idKorisnik = user[0].idkorisnik;
    const sqlAddMember =
      `INSERT INTO clanKluba (datumUpisa, idKorisnik)
      VALUES ('` +
      date +
      `', ` +
      idKorisnik +
      `)`;
    console.log(user[0].idkorisnik);
    await db.query(sqlAddMember);
    const sqlMember =
      `SELECT brclanskeiskaznice FROM clankluba WHERE idkorisnik = ` +
      idKorisnik;
    const member = (await db.query(sqlMember, [])).rows;
    brclanskeiskaznice = member[0].brclanskeiskaznice;
    console.log(req.body.dances);
    for (var i = 0; i < req.body.dances.length; i++) {
      console.log(req.body.dances[i]);
      const sqlInsertUpisuje =
        `INSERT INTO upisuje (brclanskeiskaznice, idstil, idmjesec) VALUES (` +
        brclanskeiskaznice +
        `,` +
        req.body.dances[i].idstil +
        `, ` +
        idMjesec +
        `)`;
      await db.query(sqlInsertUpisuje);
    }
    const err = { err: false };
    res.json(err);
  } catch (e) {
    console.log(e);
    const err = { err: true, errmessage: e };
    res.json(err);
  }
});

router.delete("/id/:id", async function (req, res, next) {
  console.log("pozvan delete");
  console.log(req.params.id);

  try {
    console.log("U try");
    const sql =
      `SELECT idKorisnik FROM korisnik WHERE email ='` + req.params.id + `'`;
    const user = (await db.query(sql, [])).rows;
    console.log(user);
    idKorisnik = user[0].idkorisnik;
    console.log(idKorisnik);
    const sqlDelete = `DELETE FROM korisnik * WHERE idKorisnik =` + idKorisnik;
    await db.query(sqlDelete);
    const error = { err: false };
    res.json(error);
  } catch (err) {
    const error = { err: true };
    res.json(error);
  }
});

router.get("/id/:id", async (req, res) => {
  console.log("id:");
  var email = req.params.id;
  console.log(email);

  const sqlMember =
    `SELECT ime, prezime,idmjesec, nazivmjesec, paid, cijena FROM korisnik NATURAL JOIN clankluba NATURAL JOIN
    mjesec NATURAL JOIN placa NATURAL JOIN clanarina WHERE email = '` +
    email +
    `'`;
  try {
    const member = (await db.query(sqlMember, [])).rows;
    res.json(member);
  } catch (err) {
    console.log(err);
  }
});

router.put("/id/:id", async function (req, res, next) {
  console.log("u putu");
  console.log(req.params.id);
  console.log(req.body.membership);
  const sqlSelect =
    `SELECT brClanskeIskaznice FROM 
    korisnik NATURAL JOIN clankluba WHERE email = '` +
    req.params.id +
    `'`;
  try {
    // await db.query(sqlUpdate);
    const user = (await db.query(sqlSelect, [])).rows;
    const brClanskeIskaznice = user[0].brclanskeiskaznice;
    for (var i = 0; i < req.body.membership.length; i++) {
      console.log(req.body.membership[i]);
      if (req.body.membership[i].paid === true) {
        const sqlUpdate =
          `UPDATE placa SET paid = true WHERE brclanskeiskaznice =` +
          brClanskeIskaznice +
          ` AND idMjesec = ` +
          req.body.membership[i].idmjesec;
        await db.query(sqlUpdate);
      }
    }

    const err = { err: false };
    res.json(err);
    // res.json(user[0]);
  } catch (e) {
    console.log(e);
    const err = { err: true };
    res.json(err);
  }
});
module.exports = router;
