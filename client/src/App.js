import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";
import Header from "./components/Header";
import HomePage from "./pages/HomePage";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Competitions from "./pages/Competitions";
import CompetitionById from "./pages/CompetitionById";
import CompetitionSign from "./components/CompetitionSign";
import Membership from "./pages/Membership";
import Members from "./pages/Members";
import MemberAdd from "./pages/MemberForm";
import Trainers from "./pages/Trainers";
import TrainerAdd from "./pages/TrainerForm";
import Dance from "./pages/Dance";
import MemberById from "./pages/MemberById";
import CompetitionsAdd from "./pages/CompetitionForm";
import Training from "./pages/Trainings";
import TrainingById from "./pages/TrainingById";
import TrainingForm from "./pages/TrainingForm";

class App extends React.Component {
  render() {
    return (
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/">
            <Header />
            <CompetitionSign />
            <HomePage />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route path="/login">
            <Login />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/profile">
            <Profile />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route path="/competitions/id">
            <CompetitionById />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/competitions">
            <Header />
            <Competitions />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/competitions/add">
            <Header />
            <CompetitionsAdd />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/membership">
            <Membership />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/membership/dance">
            <Dance />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/members">
            <Members />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route path="/members/id">
            <MemberById />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/members/add">
            <MemberAdd />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/trainers">
            <Trainers />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/trainers/add">
            <TrainerAdd />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/training">
            <Training />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route path="/training/id">
            <TrainingById />
            <Footer />
          </Route>
        </Switch>
        <Switch>
          <Route exact path="/training/add">
            <Header />
            <TrainingForm />
            <Footer />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
