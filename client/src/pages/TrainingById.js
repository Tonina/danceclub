import React, { Component } from "react";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import { Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import RotateRightIcon from "@material-ui/icons/RotateRight";

class TrainingById extends Component {
  state = {
    training: [],
    loading: true,
    members: [],
    danceStyle: "",
    capacity: "",
    duration: "",
    user: [],
  };

  componentDidMount() {
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const trainingId = currentPath.slice(index + 1, currentPath.length);
    fetch("/training/id/" + trainingId)
      .then((res) => res.json())
      .then((training) => {
        this.setState({ loading: false });
        this.setState({ training }, () => console.log(training));
      });

    fetch("/training/user/" + trainingId)
      .then((res) => res.json())
      .then((user) => {
        this.setState({ user }, () => console.log(user));
      });
  }

  getName() {
    let n = "";
    let s = "";
    for (let index = 0; index < this.state.training.length; index++) {
      n = this.state.training[index].ime;
      s = this.state.training[index].prezime;
    }
    this.state.name = n;
    this.state.surname = s;
  }

  caluclateMembers() {
    let help = [];
    let d = "";
    let s = "";
    let c = "";
    let name = "";
    let surname = "";

    for (var i = 0; i < this.state.training.length; i++) {
      name = this.state.training[i].ime;
      surname = this.state.training[i].prezime;
      let data = { name: name, surname: surname };
      d = this.state.training[i].trajanje;
      c = this.state.training[i].kapacitet;
      s = this.state.training[i].nazivstil;
      help.push(data);
    }

    this.state.members = help;
    this.state.danceStyle = s;
    this.state.capacity = c;
    this.state.duration = d;
    //this.setState({ danceStyle: s });
    //this.setState({ capacity: c });
    //this.setState({ duration: d });
    //this.setState({ members: help });

    console.log(this.state.duration);
    console.log(this.state.members);
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            backgroundColor: "#e8f4fc",
            padding: "10%",
            textAlign: "center",
            fontSize: "3vw",
            fontStyle: "italic",
          }}
        >
          <RotateRightIcon style={{ fontSize: "3vw" }} />
          Loading...
        </div>
      );
    }

    this.caluclateMembers();

    return (
      <div
        style={{
          border: "2px double #830981",
          width: "30%",
          margin: "auto",
          marginTop: "3em",
          backgroundColor: "#FFF0F5",
        }}
      >
        <Row backgroundColor="white">
          <h2
            style={{
              padding: "1em",
              textAlign: "center",
              color: "#8d2060",
              margin: "auto",
            }}
          >
            {" "}
            Trening je održao/la: {this.state.user.ime}{" "}
            {this.state.user.prezime}
          </h2>
        </Row>

        <div style={{ textAlign: "center", fontSize: "1.2em" }}>
          Plesali su{" "}
          <span style={{ color: "#8d2060" }}>{this.state.danceStyle}</span>:
          {this.state.members.map((m) => (
            <div>
              - {m.name} {m.surname}
            </div>
          ))}
        </div>
        <div
          style={{ textAlign: "center", fontSize: "1.2em", marginTop: "1em" }}
        >
          Popunjenost kapaciteta dvorane : {this.state.user.count}/
          {this.state.capacity}
        </div>

        <Link
          to={"/training"}
          style={{
            margin: "auto",
            color: "black",
            fontSize: "1.5vw",
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
            marginTop: "1em",
            marginBottom: "1em",
          }}
        >
          <KeyboardReturnIcon /> Vrati se na popis treninga
        </Link>
      </div>
    );
  }
}
export default TrainingById;
