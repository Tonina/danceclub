import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import InputLabel from "@material-ui/core/InputLabel";

export class MemberFormDetails extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    oib: "",
    datebirth: "",
    facultydegree: false,
    phone: "",
  };

  postMethod() {
    console.log(this.state.firstName);
    const data = {
      name: this.state.firstName,
      surname: this.state.lastName,
      email: this.state.email,
      oib: this.state.oib,
      pass: this.state.password,
      datebirth: this.state.datebirth,
      facultydegree: this.state.facultydegree,
      phone: this.state.phone,
    };
    fetch("/trainers/add", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "localhost:5000",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.err === true) {
          console.log(data.errmessage);
          alert("Error while adding new trainer!\n" + data.errmessage.detail);
          console.log("usao u err");
          window.location.replace("/trainers");
        } else {
          alert("New trainer added!");
          window.location.replace("/trainers");
        }
      })
      .catch((error) => {
        alert("Error while adding new trainer!");
        console.error("Error:", error);
        window.location.replace("/trainers");
      })
      .catch(function (err) {
        alert("Error while adding new trainer!");
        console.error("Error:", err);
        window.location.replace("/trainers");
      });
  }

  handleChange = (input) => (e) => {
    this.setState({ [input]: e.target.value });
    console.log(input);
  };

  handleDegree = () => {
    let b = this.state.facultydegree;
    this.state.facultydegree = !b;
    console.log(this.state.facultydegree);
  };

  render() {
    return (
      <div>
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
            textAlign: "center",
          }}
        >
          Osobni podaci trenera:
        </h2>
        <p style={{ margin: "auto", textAlign: "center", fontSize: "1em" }}>
          Potvrdi gumb će biti onemogućen sve dok obavezna polja nisu ispunjena
          <br />
          (ili je duljina OIB-a drugačija od 11 ili broj mobitela ima manje ili
          više od 10 brojeva).
        </p>
        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <AppBar title="Unesi podatke za trenera" />
              <TextField
                required
                placeholder="Unesite ime"
                label="Ime"
                onChange={this.handleChange("firstName")}
                defaultValue={this.state.firstName}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                placeholder="Unesite prezime"
                label="Prezime"
                onChange={this.handleChange("lastName")}
                defaultValue={this.state.lastName}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                placeholder="Unestite email adresu"
                label="Email adresa"
                onChange={this.handleChange("email")}
                defaultValue={this.state.email}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                type="number"
                placeholder="Unesite OIB"
                label="OIB"
                onChange={this.handleChange("oib")}
                defaultValue={this.state.oib}
                margin="normal"
                fullWidth
              />
              <br />
              <br />
              <InputLabel>Datum rođenja*</InputLabel>
              <TextField
                required
                type="date"
                onChange={this.handleChange("datebirth")}
                defaultValue={this.state.datebirth}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                type="number"
                placeholder="Unesite broj mobitela"
                label="Broj mobitela"
                onChange={this.handleChange("phone")}
                defaultValue={this.state.phone}
                margin="normal"
                fullWidth
              />
              <br />

              <TextField
                required
                type="password"
                secureTextEntry={true}
                placeholder="Unesite lozinku"
                label="Lozinka"
                onChange={this.handleChange("password")}
                defaultValue={this.state.password}
                margin="normal"
                fullWidth
              />
              <br />
              <Form.Check
                type="checkbox"
                label="Završen kineziološki fakultet"
                onChange={this.handleDegree.bind()}
                defaultChecked={this.state.facultydegree}
              />
              <br />
              <Button
                color="secondary"
                variant="contained"
                onClick={this.postMethod.bind(this)}
                disabled={
                  this.state.email.length < 1 ||
                  this.state.oib.length !== 11 ||
                  this.state.firstName.length < 1 ||
                  this.state.lastName.length < 1 ||
                  this.state.password.length < 1 ||
                  this.state.phone.length !== 10 ||
                  this.state.datebirth === ""
                }
                style={{ marginBottom: "1em" }}
              >
                Potvrdi i dodaj trenera
              </Button>
              <Link to="/trainers">
                <Button
                  variant="contained"
                  style={{ marginLeft: "1em", marginBottom: "1em" }}
                >
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default MemberFormDetails;
