import React, { useState } from "react";
import { Button, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./css/Login.css";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import { useHistory } from "react-router-dom";

function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  let history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      email: email,
      password: password,
    };
    fetch("/login", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.err) {
          alert("Invalid login! Try again!");
          history.push("/login");
          history.go(0);
        } else {
          console.log("Success:", data);
          localStorage.setItem("id", data.idkorisnik);
          localStorage.setItem("name", data.ime);
          localStorage.setItem("surname", data.prezime);
          localStorage.setItem("role", data.nazivuloga);
          localStorage.setItem("email", data.email);
          history.push("/");
          history.go(0);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("Invalid login! Try again!");
        history.push("/login");
        history.go(0);
      })
      .catch(function (err) {
        alert("Invalid login! Try again!");
        history.push("/login");
        history.go(0);
      });
  };

  return (
    <Form className="wrapper">
      <div
        style={{
          width: "20%",
          margin: "auto",
          marginTop: "5em",
          backgroundColor: "#FFF0F5",
          padding: "3%",
          borderRadius: "4px",
          border: "solid",
        }}
      >
        <h2
          style={{
            textAlign: "center",
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
            fontSize: "2em ",
          }}
        >
          Prijavi se
          <VpnKeyIcon />
        </h2>
        <Form.Group controlId="formBasicEmail">
          <Form.Label
            className="label"
            style={{ fontSize: "1em", fontWeight: "bold" }}
          >
            Email adresa:{" "}
          </Form.Label>
          <Form.Control
            style={{ fontSize: "1em", width: "100%" }}
            className="textBox"
            type="email"
            placeholder="Unesite email adresu"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label
            className="label"
            style={{ fontSize: "1em", fontWeight: "bold" }}
          >
            Lozinka:{" "}
          </Form.Label>
          <Form.Control
            style={{ fontSize: "1em", width: "100%" }}
            className="textBox"
            type="password"
            placeholder="Unesite lozinku"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
        </Form.Group>
        <div>
          <Button
            className="submit-login-signin"
            expand="lg"
            bg="dark"
            variant="dark"
            type="submit"
            onClick={(event) => handleSubmit(event)}
          >
            Prijavi se
          </Button>
        </div>
      </div>
    </Form>
  );
}
export default Login;
