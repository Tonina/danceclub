import React, { Component } from "react";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import { Button, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import RotateRightIcon from "@material-ui/icons/RotateRight";
import { Checkbox } from "@material-ui/core";
import "./css/MemberById.css";

class MemberById extends Component {
  state = {
    membership: [],
    loading: true,
    name: "",
    surname: "",
    d: true,
  };

  componentDidMount() {
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const memberEmail = currentPath.slice(index + 1, currentPath.length);
    fetch("/members/id/" + memberEmail)
      .then((res) => res.json())
      .then((membership) => {
        this.setState({ loading: false });
        this.setState({ membership }, () => console.log(membership));
      });
  }

  handleClick = () => {
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const memberEmail = currentPath.slice(index + 1, currentPath.length);
    let data = { membership: this.state.membership };
    console.log("u click");
    console.log(data);
    fetch("/members/id/" + memberEmail, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "localhost:5000",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
        window.location.replace("/members");
      })
      .catch((error) => {
        console.error("Error:", error);
        window.location.replace("/members");
      });
  };

  handleDances = (input) => (e) => {
    let danceHelp = [];
    this.state.membership.map((m) => {
      if (m.idmjesec === input.m.idmjesec) {
        input.m.paid = !m.paid;
        danceHelp.push(input.m);
      } else danceHelp.push(m);
    });
    this.state.membership = danceHelp;
  };

  getName() {
    let n = "";
    let s = "";
    for (let index = 0; index < this.state.membership.length; index++) {
      n = this.state.membership[index].ime;
      s = this.state.membership[index].prezime;
    }
    this.state.name = n;
    this.state.surname = s;
  }

  render() {
    this.getName();
    for (let index = 0; index < this.state.membership.length; index++) {
      if (this.state.membership[index].paid === false) {
        this.state.d = false;
      }
    }

    if (this.state.loading) {
      return (
        <div
          style={{
            backgroundColor: "#e8f4fc",
            padding: "10%",
            textAlign: "center",
            fontSize: "3vw",
            fontStyle: "italic",
          }}
        >
          <RotateRightIcon style={{ fontSize: "3vw" }} />
          Loading...
        </div>
      );
    }

    return (
      <div
        style={{
          border: "2px double #830981",
          width: "30%",
          margin: "auto",
          marginTop: "3em",
          backgroundColor: "#FFF0F5",
        }}
      >
        <Row backgroundColor="white">
          <h2
            style={{
              padding: "1em",
              textAlign: "center",
              color: "#8d2060",
              margin: "auto",
            }}
          >
            {" "}
            ČLAN : {this.state.name} {this.state.surname}
          </h2>
        </Row>

        <div style={{ textAlign: "center", fontSize: "1.5em" }}>
          {this.state.membership.map((m) => (
            <div>
              <Checkbox
                value={m.idmjesec}
                defaultChecked={m.paid}
                disabled={
                  (m.paid === true) |
                    (localStorage.getItem("role") === "trener") && true
                }
                onClick={this.handleDances({ m })}
              />
              {m.nazivmjesec}: {m.cijena} kn
            </div>
          ))}
        </div>
        {localStorage.getItem("role") === "vlasnik" && (
          <Button
            className="payment"
            onClick={this.handleClick}
            disabled={this.state.d}
          >
            Potvrdi plaćanje
          </Button>
        )}
        <Link
          to={"/members"}
          style={{
            margin: "auto",
            color: "black",
            fontSize: "1.5vw",
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
            marginTop: "1em",
            marginBottom: "1em",
          }}
        >
          <KeyboardReturnIcon /> Vrati se na popis članova kluba
        </Link>
      </div>
    );
  }
}
export default MemberById;
