import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import "./css/Members.css";
import paginationFactory from "react-bootstrap-table2-paginator";
import DoneIcon from "@material-ui/icons/Done";
import ClearIcon from "@material-ui/icons/Clear";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";

class Trainers extends Component {
  state = {
    trainers: [],
  };

  componentDidMount() {
    fetch("/trainers")
      .then((res) => res.json())
      .then((trainers) =>
        this.setState({ trainers }, () => console.log(trainers))
      );
  }

  deleteTrainer(e, id) {
    // Simple DELETE request with fetch
    fetch("/trainers/id/" + e, { method: "DELETE" })
      .then((res) => res.json())
      .then((data) => {
        if (data.err === true) {
          alert("Error while deleting trainer!\n" + data.errmessage.detail);
          window.location.reload(false);
        } else {
          alert("Trainer deleted!");
          window.location.reload(false);
        }
      })
      .catch((err) => console.log(err));
  }

  render() {
    for (let i = 0; i < this.state.trainers.length; i++) {
      if (this.state.trainers[i].oznakakinfakultet === true) {
        this.state.trainers[i].oznakakinfakultet = <DoneIcon />;
      } else {
        this.state.trainers[i].oznakakinfakultet = <ClearIcon />;
      }
    }
    return (
      <div className="wrapp">
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            margin: "auto",
            width: "50%",
            marginBottom: "1rem",
            marginTop: "5em",
            height: "100%",
          }}
        >
          <h2>Treneri:</h2>
          <Table
            style={{
              borderCollapse: "collapse",
              borderSpacing: "0 15px",
              backgroundColor: "whitesmoke",
            }}
            pagination={paginationFactory({ sizePerPage: 5 })}
          >
            <thead>
              <tr style={{ border: "solid black 1px" }}>
                <th>Ime</th>
                <th>Prezime</th>
                <th>Email adresa</th>
                <th>Datum rođenja</th>
                <th>Broj mobitela</th>
                <th id="bool">Završen kineziološki fakultet</th>
                <th>Obriši</th>
              </tr>
            </thead>
            <tbody>
              {this.state.trainers.map((t) => (
                <tr key={t.email}>
                  {Object.values(t).map((val) => (
                    <td>{val}</td>
                  ))}
                  <td key="button">
                    {" "}
                    <Button onClick={this.deleteTrainer.bind(this, t.email)}>
                      <DeleteIcon />
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            marginTop: "3rem",
            marginBottom: "1em",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
            width: "20%",
          }}
        >
          <Link
            to="/trainers/add"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              fontSize: "1.5em",
            }}
          >
            Dodaj novog trenera! <PersonAddIcon />
          </Link>
        </div>
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",

            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
            width: "20%",
          }}
        >
          <Link
            to="/training"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              fontSize: "1.5em",
            }}
          >
            Vidi sve treninge!
          </Link>
        </div>
      </div>
    );
  }
}
export default Trainers;
