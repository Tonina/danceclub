import React, { Component } from "react";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import { Row, Col, Media } from "react-bootstrap";
import { Link } from "react-router-dom";
import medal from "../images/medal.png";
import RotateRightIcon from "@material-ui/icons/RotateRight";

class CompetitionById extends Component {
  state = {
    competition: [],
    loading: true,
  };

  componentDidMount() {
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const competitionId = currentPath.slice(index + 1, currentPath.length);
    fetch("/competitions/id/" + competitionId)
      .then((res) => res.json())
      .then((competition) => {
        this.setState({ loading: false });
        this.setState({ competition }, () => console.log(competition));
      });
  }

  getName() {
    let n = "";
    for (let index = 0; index < this.state.competition.length; index++) {
      n = this.state.competition[index].nazivnat;
    }
    this.state.name = n;
    //this.setState({ name: n });
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            backgroundColor: "#e8f4fc",
            padding: "10%",
            textAlign: "center",
            fontSize: "3vw",
            fontStyle: "italic",
          }}
        >
          <RotateRightIcon style={{ fontSize: "3vw" }} />
          Loading...
        </div>
      );
    }
    this.getName();
    console.log(this.state.competition);
    console.log(this.state.name);

    return (
      <div>
        <Row backgroundColor="white">
          <h2
            style={{
              padding: "1em",
              textAlign: "center",
              color: "black",
              fontize: "30px",
              fontFamily: "Lucida Handwriting",
              width: "30%",
              margin: "auto",
              marginTop: "3em",
            }}
          >
            {this.state.name}
            <img src={medal} alt="medals" style={{ width: "30%" }}></img>
          </h2>
        </Row>
        <Link
          to={"/competitions"}
          style={{
            margin: "auto",
            color: "black",
            fontSize: "1.5vw",
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <KeyboardReturnIcon /> Vrati se na popis natjecanja!
        </Link>

        {this.state.competition.map((c) => {
          return (
            <div
              style={{
                paddingTop: "1.5rem",
                paddingBottom: "0.3rem",
              }}
            >
              <Media
                style={{
                  backgroundColor: "whitesmoke",
                  margin: "auto",
                  padding: "1%",
                  border: "1px solid darkolivegreen",
                  width: "30%",
                }}
              >
                <Media.Body>
                  <Row>
                    <Col xs={6}>
                      <strong style={{ fontSize: "1.3vw" }}>
                        {c.ime} {c.prezime}
                      </strong>
                    </Col>
                    <Col xs={6}>
                      <strong style={{ fontSize: "1.3vw" }}>
                        {" "}
                        osvojio/la {c.plasman}. mjesto u {c.nazivstil}-u
                      </strong>
                    </Col>
                  </Row>
                </Media.Body>
              </Media>
            </div>
          );
        })}
      </div>
    );
  }
}
export default CompetitionById;
