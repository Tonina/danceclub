import React, { Component } from "react";
import FormDancesMembership from "../components/danceform/FormDancesMembership";
import ConfirmDance from "../components/danceform/ConfirmDance";
import SuccessDance from "../components/danceform/SuccessDance";

export class DanceForm extends Component {
  state = {
    step: 1,
    id: localStorage.getItem("id"),
    dances: [],
    isSuccessful: false,
  };

  componentDidMount() {
    fetch("/dances")
      .then((res) => res.json())
      .then((dances) => this.setState({ dances }, () => console.log(dances)));
  }

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };

  // Handle fields change

  handleError = () => (e) => {
    console.log("u handle error");
    this.setState({ isSuccessful: true });
    console.log(this.state.isSuccessful);
  };
  handleDances = (input) => (e) => {
    let danceHelp = [];
    for (var i = 0; i < this.state.dances.length; i++) {
      if (this.state.dances[i].idstil === input.d.idstil) {
        input.d.isdance = !this.state.dances[i].isdance;
        danceHelp.push(input.d);
      } else danceHelp.push(this.state.dances[i]);
    }
    this.setState({ dances: danceHelp });
    //this.state.dances = danceHelp;
    console.log(this.state.dances);
  };

  render() {
    const { step } = this.state;
    const { id, dances, isSuccessful } = this.state;
    const values = {
      id,
      dances,
      isSuccessful,
    };

    switch (step) {
      case 1:
        return (
          <FormDancesMembership
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            handleDances={this.handleDances}
            values={values}
          />
        );
      case 2:
        return (
          <ConfirmDance
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            values={values}
          />
        );
      case 3:
        return (
          <SuccessDance
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            values={values}
          />
        );

      default:
        console.log("This is a multi-step form built with React.");
    }
  }
}

export default DanceForm;
