import React, { Component } from "react";
import TrainingFormDetails from "../components/trainingform/TrainingFormDetails";
import TrainingParticipants from "../components/trainingform/TrainingParticipants";
import ConfirmTraining from "../components/trainingform/ConfirmTraining";

export class TrainingForm extends Component {
  state = {
    step: 1,
    data: [],
    trainingDate: "",
    start: "",
    end: "",
    dancinghall: "",
    hallSelected: [],
    danceSelected: [],
    dance: "",
    isSuccessful: false,
  };

  componentDidMount() {
    fetch("/training/add")
      .then((res) => res.json())
      .then((data) => this.setState({ data }, () => console.log(data)));
  }

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };

  // Handle fields change
  handleChange = (input) => (e) => {
    this.setState({ [input]: e.target.value });
  };

  handleError = () => (e) => {
    this.setState({ isSuccessful: true });
    // this.state.isSuccessful = true;
  };

  handleDance = (event) => {
    let help = [];
    for (let i = 0; i < this.state.data.members.length; i++) {
      let osoba = this.state.data.members[i].osoba;
      let idkorisnik = this.state.data.members[i].idkorisnik;
      let has = false;
      let idmjesec = this.state.data.members[i].idmjesec;
      let idstil = this.state.data.members[i].idstil;

      let d = {
        osoba: osoba,
        idkorisnik: idkorisnik,
        hasparticipated: has,
        idmjesec: idmjesec,
        idstil: idstil,
      };
      help.push(d);
    }
    this.state.data.members = help;
    this.state.dance = event.label;
    this.state.danceSelected = event;
  };

  handleHall = (e) => {
    this.state.dancinghall = e.label;
    this.state.hallSelected = e;
  };

  handleParticipants = (input) => (e) => {
    console.log(input);
    console.log(e.target);
    let help = [];
    let date =
      this.state.trainingDate.substring(0, 4) +
      this.state.trainingDate.substring(5, 7);

    for (var i = 0; i < this.state.data.members.length; i++) {
      if (
        this.state.data.members[i].idkorisnik === input.m1.idkorisnik &&
        this.state.data.members[i].idstil == this.state.danceSelected.value &&
        this.state.data.members[i].idmjesec == date
      ) {
        input.m1.hasparticipated = !this.state.data.members[i].hasparticipated;
        help.push(input.m1);
      } else {
        if (
          this.state.data.members[i].idstil == this.state.danceSelected.value &&
          this.state.data.members[i].idmjesec == date
        ) {
          console.log(this.state.data.members[i]);
          help.push(this.state.data.members[i]);
        } else {
          console.log(this.state.data.members[i]);
          input.m1.hasparticipated =
            !this.state.data.members[i].hasparticipated;
          help.push(input.m1);
        }
      }
    }
    this.setState({ members: help });
  };

  render() {
    const { step } = this.state;
    const {
      trainingDate,
      start,
      end,
      data,
      dance,
      dancinghall,
      hallSelected,
      danceSelected,
      isSuccessful,
    } = this.state;
    const values = {
      trainingDate,
      start,
      end,
      data,
      dance,
      dancinghall,
      hallSelected,
      danceSelected,
      isSuccessful,
    };

    switch (step) {
      case 1:
        return (
          <TrainingFormDetails
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            handleDance={this.handleDance}
            handleHall={this.handleHall}
            values={values}
          />
        );
      case 2:
        return (
          <TrainingParticipants
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            handleParticipants={this.handleParticipants}
            values={values}
          />
        );
      case 3:
        return (
          <ConfirmTraining
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            values={values}
          />
        );
      default:
        console.log("This is a multi-step form built with React.");
    }
  }
}

export default TrainingForm;
