import React, { Component } from "react";
import FormUserDetails from "../components/memberform/MemberFormDetails";
import FormDances from "../components/memberform/FormDances";
import Confirm from "../components/memberform/MemberFormConfirm";
import Success from "../components/memberform/Success";

export class MemberForm extends Component {
  state = {
    step: 1,
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    oib: "",
    datebirth: "",
    dances: [],
    isSuccessful: false,
  };

  componentDidMount() {
    fetch("/dances")
      .then((res) => res.json())
      .then((dances) => this.setState({ dances }, () => console.log(dances)));
  }

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };

  // Handle fields change
  handleChange = (input) => (e) => {
    this.setState({ [input]: e.target.value });
    console.log(input);
  };
  handleError = () => (e) => {
    console.log("u handle error");
    this.state.isSuccessful = true;
    console.log(this.state.isSuccessful);
  };
  handleDances = (input) => (e) => {
    let danceHelp = [];
    this.state.dances.map((d) => {
      if (d.idstil === input.d.idstil) {
        input.d.isdance = !d.isdance;
        danceHelp.push(input.d);
      } else danceHelp.push(d);
    });
    this.state.dances = danceHelp;
  };

  render() {
    const { step } = this.state;
    const {
      firstName,
      lastName,
      email,
      password,
      oib,
      datebirth,
      dances,
      isSuccessful,
    } = this.state;
    const values = {
      firstName,
      lastName,
      email,
      password,
      oib,
      datebirth,
      dances,
      isSuccessful,
    };

    switch (step) {
      case 1:
        return (
          <FormUserDetails
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 2:
        return (
          <FormDances
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            handleDances={this.handleDances}
            values={values}
          />
        );
      case 3:
        return (
          <Confirm
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            values={values}
          />
        );
      case 4:
        return (
          <Success
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            values={values}
          />
        );

      default:
        console.log("This is a multi-step form built with React.");
    }
  }
}

export default MemberForm;
