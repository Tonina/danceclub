import React, { useState } from "react";
import { Form, Button, Card, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import DoneOutlineIcon from "@material-ui/icons/DoneOutline";
import SyncIcon from "@material-ui/icons/Sync";
import "./css/Profile.css";
import PaymentIcon from "@material-ui/icons/Payment";
import GroupIcon from "@material-ui/icons/Group";

function Profil() {
  const [firstName, setFirstName] = useState(localStorage.getItem("name"));
  const [lastName, setLastName] = useState(localStorage.getItem("surname"));
  const [email, setEmail] = useState(localStorage.getItem("email"));
  const [dances, setDances] = useState([]);
  const id = localStorage.getItem("id");

  const [changed, setChange] = useState(false);

  const [errFirstName, setErrFirstName] = useState("");
  const [errLastName, setErrLastName] = useState("");
  const [errEmail, setErrEmail] = useState("");

  console.log(firstName);
  console.log(errFirstName);

  async function handleSubmit() {
    let data = { name: firstName, surname: lastName, email: email, id: id };
    if (firstName === "") setErrFirstName("Molimo vas unesite ime!");
    else setErrFirstName("");
    if (lastName === "") setErrLastName("Molimo vas unesite prezime!");
    else setErrLastName("");
    if (email === "") setErrEmail("Molimo vas unesite email adresu!");
    else setErrEmail("");
    if (firstName !== "" && lastName !== "" && email !== "") {
      fetch("/profile", {
        method: "PUT", // or 'PUT'
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "localhost:5000",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Success:", data);
          localStorage.setItem("id", data.idkorisnik);
          localStorage.setItem("name", data.ime);
          localStorage.setItem("surname", data.prezime);
          localStorage.setItem("role", data.nazivuloga);
          localStorage.setItem("email", data.email);
          setChange(true);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  }

  React.useEffect(() => {
    console.log("u useeffect");
    fetch("/profile/id/" + id)
      .then((res) => res.json())
      .then((data) => setDances(data))
      .catch((e) => {
        console.error("Error: ", e);
      });
  }, []);

  const styles = {
    backgroundColor: "whitesmoke",
    padding: "10%",
    marginTop: "3.2rem",
  };

  if (changed) {
    return (
      <div className="loading" style={styles}>
        <Row
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <h1
            style={{
              color: "black",
              display: "flex",
              alignItems: "center",
              flexWrap: "wrap",
              justifyContent: "center",
            }}
          >
            Izmijenjeno!{" "}
            <DoneOutlineIcon
              fontSize="large"
              style={{ marginBottom: "0.5rem" }}
            />{" "}
          </h1>
        </Row>
        <Row style={{ display: "flex", justifyContent: "center" }}>
          <Link
            to="/profile"
            onClick={() => window.location.reload()}
            style={{
              color: "black",
              display: "flex",
              alignItems: "center",
              flexWrap: "wrap",
              justifyContent: "center",
              fontSize: "1em",
            }}
          >
            <KeyboardBackspaceIcon /> Vrati se na profil!
          </Link>
        </Row>
      </div>
    );
  }

  return (
    <div
      className="wrapper"
      style={{
        marginTop: "3rem",
        display: "flex",
        justifyContent: "center",
        height: "50%",
        flexDirection: "column",
      }}
    >
      <Card
        className="profile-box"
        style={{
          margin: "auto",
          backgroundColor: "whitesmoke",
          width: "37rem",
          marginTop: "3rem",
          marginBottom: "2rem",
          border: "solid black",
        }}
      >
        <Card.Header
          style={{
            display: "flex",
            justifyContent: "center",
            opacity: 0.9,
            backgroundColor: "#8d2060",
            fontSize: "1.5em",
            color: "white",
          }}
        >
          <strong
            style={{
              fontSize: "2.5vw",
            }}
          >
            Profil
          </strong>
        </Card.Header>
        <Card.Body style={{ textAlign: "center" }}>
          <PersonPinIcon
            style={{ fontSize: "10em", marginTop: "5%", color: "#303030" }}
          />
          <Card.Text style={{ fontSize: "1.5em", textDecoration: "underline" }}>
            {firstName} {lastName}
          </Card.Text>
        </Card.Body>
        <Card>
          <Card.Body>
            <Form.Group className="profil-data">
              <Form.Row style={{ marginBottom: "2rem" }}>
                <div style={{ marginLeft: "10em", marginBottom: "1rem" }}>
                  <Form.Label htmlFor="fname" className="lab">
                    Ime:
                  </Form.Label>
                  <Form.Control
                    className="change-data"
                    type="text"
                    placeholder="Ime"
                    defaultValue={localStorage.getItem("name")}
                    onChange={(event) => setFirstName(event.target.value)}
                  />
                  <div
                    className="text-danger"
                    style={{
                      color: "red",
                      fontStyle: "italic",
                      marginLeft: "2em",
                    }}
                  >
                    {errFirstName}
                  </div>
                </div>
                <div style={{ marginLeft: "10rem", marginBottom: "1rem" }}>
                  <Form.Label htmlFor="lname" className="lab">
                    Prezime:
                  </Form.Label>
                  <Form.Control
                    className="change-data"
                    type="text"
                    placeholder="Prezime"
                    defaultValue={localStorage.getItem("surname")}
                    onChange={(event) => setLastName(event.target.value)}
                  />
                  <div
                    className="text-danger"
                    style={{
                      color: "red",
                      fontStyle: "italic",
                      marginLeft: "2em",
                    }}
                  >
                    {errLastName}
                  </div>
                </div>
                <div style={{ marginLeft: "10rem", marginBottom: "2rem" }}>
                  <Form.Label htmlFor="" className="lab">
                    E-mail adresa:
                  </Form.Label>
                  <Form.Control
                    className="change-data"
                    type="text"
                    placeholder="E-mail adresa"
                    defaultValue={localStorage.getItem("email")}
                    onChange={(event) => setEmail(event.target.value)}
                  />
                  <div
                    className="text-danger"
                    style={{
                      color: "red",
                      fontStyle: "italic",
                      marginLeft: "2em",
                    }}
                  >
                    {errEmail}
                  </div>
                </div>
              </Form.Row>
              <Link
                to="/profile"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  textDecoration: "none",
                }}
              >
                <Button
                  className="change"
                  expand="lg"
                  bg="dark"
                  variant="dark"
                  type="submit"
                  onClick={() => handleSubmit()}
                >
                  Izmijeni <SyncIcon />
                </Button>
              </Link>
            </Form.Group>
          </Card.Body>
        </Card>
      </Card>
      {localStorage.getItem("role") === "vlasnik" && (
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            width: "37rem",
            marginBottom: "3rem",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
          }}
        >
          <Link
            to="/trainers"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            Treneri
            <GroupIcon />
          </Link>
          <Link
            to="/members"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            Članovi kluba
            <GroupIcon />
          </Link>
        </div>
      )}
      {localStorage.getItem("role") === "trener" && (
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            width: "37rem",
            marginBottom: "3rem",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
          }}
        >
          <Link
            to="/training"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            Treninzi
            <GroupIcon />
          </Link>
          <Link
            to="/members"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            Članovi kluba
            <GroupIcon />
          </Link>
        </div>
      )}
      {localStorage.getItem("role") === "clan" && (
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            width: "37rem",
            marginBottom: "3rem",
            border: "2px double #830981",
            borderRadius: "25px",
          }}
        >
          <ul
            style={{
              margin: "auto",
              textAlign: "center",
              listStyleType: "none",
            }}
          >
            Ovaj mjesec upisan na:{" "}
            {dances.map((c) => {
              return <li>{c.nazivstil} </li>;
            })}
          </ul>
          <Link
            to="/membership"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              marginTop: "1em",
            }}
          >
            Idi na moje članarine
            <PaymentIcon />
          </Link>
        </div>
      )}
    </div>
  );
}

export default Profil;
