import React from "react";
import CompetitionComp from "../components/CompetitionComp";
import { Link } from "react-router-dom";

function Competitions() {
  return (
    <div>
      {localStorage.getItem("role") === "vlasnik" && (
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            width: "50%",
            marginBottom: "3rem",
            marginTop: "2em",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
          }}
        >
          <Link
            to="/competitions/add"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            Unesite novo natjecanje na kojem su se natjecali članovi kluba!
          </Link>
        </div>
      )}
      <CompetitionComp />
    </div>
  );
}

export default Competitions;
