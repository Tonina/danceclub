import React, { Component } from "react";
import CompetitionFormDetails from "../components/competitionform/CompetitionFormDetail";
import Competitors from "../components/competitionform/Competitors";
import ConfirmCompetition from "../components/competitionform/ConfirmCompetition";
import CompetitorDances from "../components/competitionform/CompetitorDances";

export class CompetitionForm extends Component {
  state = {
    step: 1,
    competitionName: "",
    competitionDate: "",
    city: [],
    cityName: "",
    dances: [],
    isSuccessful: false,
    data: [],
    competitiors: [],
    c: [],
  };

  componentDidMount() {
    fetch("/competitions/add")
      .then((res) => res.json())
      .then((data) => this.setState({ data }, () => console.log(data)));
    fetch("/dances")
      .then((res) => res.json())
      .then((dances) => this.setState({ dances }, () => console.log(dances)));
  }

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };

  // Handle fields change
  handleChange = (input) => (e) => {
    this.setState({ [input]: e.target.value });
  };

  handleCompetitionStyle = (input) => (e) => {
    let help = [];
    for (var i = 0; i < this.state.c.length; i++) {
      if (this.state.c[i].idkorisnik === input.comp.idkorisnik) {
        let idkorisnik = this.state.c[i].idkorisnik;
        let osoba = this.state.c[i].osoba;
        let plasman = this.state.c[i].plasman;
        let idstil = e.value;
        let value = { value: idstil, label: e.label };
        let data = {
          idkorisnik: idkorisnik,
          osoba: osoba,
          plasman: plasman,
          idstil: idstil,
          value: value,
        };
        help.push(data);
      } else help.push(this.state.c[i]);
    }
    this.state.c = help;
    //this.setState({ c: help });

    console.log(this.state.c);
  };

  handleCompetitionPlace = (input) => (e) => {
    let help = [];

    for (var i = 0; i < this.state.c.length; i++) {
      if (this.state.c[i].idkorisnik === input.comp.idkorisnik) {
        let idkorisnik = this.state.c[i].idkorisnik;
        let osoba = this.state.c[i].osoba;
        let plasman = e.target.value;
        let idstil = this.state.c[i].idstil;
        let value = this.state.c[i].value;
        let data = {
          idkorisnik: idkorisnik,
          osoba: osoba,
          plasman: plasman,
          idstil: idstil,
          value: value,
        };
        help.push(data);
      } else help.push(this.state.c[i]);
    }
    this.state.c = help;
    //this.setState({ c: help });

    console.log(this.state.c);
  };

  handleCompetitors = (input) => (e) => {
    console.log(input);
    console.log(e.target);
    let help = [];
    for (var i = 0; i < this.state.data.members.length; i++) {
      if (this.state.data.members[i].idkorisnik === input.m.idkorisnik) {
        input.m.iscompetitor = !this.state.data.members[i].iscompetitor;
        help.push(input.m);
      } else help.push(this.state.data.members[i]);
    }
    this.state.members = help;
    // this.setState({ members: help });

    let helpC = [];
    for (var j = 0; j < this.state.data.members.length; j++) {
      if (this.state.data.members[j].iscompetitor === true) {
        let idkorisnik = this.state.data.members[j].idkorisnik;
        let osoba = this.state.data.members[j].osoba;
        let plasman = "";
        let idstil = "";
        let value = { value: "", label: "" };
        let data = {
          idkorisnik: idkorisnik,
          osoba: osoba,
          plasman: plasman,
          idstil: idstil,
          value: value,
        };
        helpC.push(data);
      }
    }
    this.state.c = helpC;
    //this.setState({ c: helpC });
    console.log("ovo nam je iz c:");
    console.log(this.state.c);
  };

  handleCity = (event) => {
    //this.setState({ city: event });
    //this.setState({ cityName: event.label });
    this.state.city = event;
    this.state.cityName = event.label;
  };

  handleError = () => (e) => {
    //this.setState({ isSuccessful: true });
    this.state.isSuccessful = true;
  };

  handleDances = (input) => (e) => {
    let danceHelp = [];
    for (var i = 0; i < this.state.dances.length; i++) {
      if (this.state.dances[i].idstil === input.d.idstil) {
        input.d.isdance = !this.state.dances[i].isdance;
        danceHelp.push(input.d);
      } else danceHelp.push(this.state.dances[i]);
    }
    //  this.setState({ dances: danceHelp });
    this.state.dances = danceHelp;
  };

  render() {
    const { step } = this.state;
    const {
      competitionName,
      competitionDate,
      city,
      cityName,
      data,
      dances,
      competitiors,
      c,
      isSuccessful,
    } = this.state;
    const values = {
      competitionName,
      competitionDate,
      city,
      cityName,
      data,
      dances,
      competitiors,
      c,
      isSuccessful,
    };

    switch (step) {
      case 1:
        return (
          <CompetitionFormDetails
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            handleCity={this.handleCity}
            values={values}
          />
        );
      case 2:
        return (
          <Competitors
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            handleCompetitors={this.handleCompetitors}
            values={values}
          />
        );
      case 3:
        return (
          <CompetitorDances
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            handleCompetitors={this.handleCompetitors}
            handleCompetitionStyle={this.handleCompetitionStyle}
            handleCompetitionPlace={this.handleCompetitionPlace}
            values={values}
          />
        );
      case 4:
        return (
          <ConfirmCompetition
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleError={this.handleError}
            values={values}
          />
        );
      default:
        console.log("This is a multi-step form built with React.");
    }
  }
}

export default CompetitionForm;
