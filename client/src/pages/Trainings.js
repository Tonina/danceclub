import React from "react";
import { Link } from "react-router-dom";
import TrainingComp from "../components/TrainingComp";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";

function Trainings() {
  return (
    <div>
      {localStorage.getItem("role") === "trener" && (
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            width: "37rem",
            marginBottom: "3rem",
            marginTop: "5em",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
          }}
        >
          <Link
            to="/training/add"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            Dodajte novi trening!
          </Link>
        </div>
      )}
      {localStorage.getItem("role") === "vlasnik" && (
        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            width: "20%",
            marginBottom: "3rem",
            marginTop: "5em",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
          }}
        >
          <Link
            to="/trainers"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              flexWrap: "wrap",
              fontSize: "1.5em",
            }}
          >
            <KeyboardBackspaceIcon />
            Vrati se na popis svih trenera!
          </Link>
        </div>
      )}
      <TrainingComp />
    </div>
  );
}

export default Trainings;
