import React from "react";
import SlideShow from "../components/SlideShow";

function HomePage() {
  return (
    <div className="App">
      <SlideShow />
    </div>
  );
}

export default HomePage;
