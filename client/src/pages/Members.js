import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import "./css/Members.css";
import paginationFactory from "react-bootstrap-table2-paginator";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import InfoIcon from "@material-ui/icons/Info";

class Members extends Component {
  state = {
    members: [],
  };

  componentDidMount() {
    fetch("/members")
      .then((res) => res.json())
      .then((members) =>
        this.setState({ members }, () => console.log(members))
      );
  }

  deleteMember(e, id) {
    console.log("brisem");
    console.log(e);

    // Simple DELETE request with fetch
    fetch("/members/id/" + e, { method: "DELETE" })
      .then((res) => res.json())
      .then((data) => {
        if (data.err === true) {
          console.log(data.errmessage);
          alert("Greška prilikom brisanja člana!\n" + data.errmessage.detail);
          window.location.reload(false);
        } else {
          alert("Član obrisan!");
          window.location.reload(false);
        }
      })
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <div className="wrapp">
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            margin: "auto",
            width: "50%",
            marginBottom: "1rem",
            marginTop: "5em",
            height: "100%",
          }}
        >
          <h2>Članovi kluba:</h2>
          <Table
            style={{
              borderCollapse: "collapse",
              borderSpacing: "0 15px",
              backgroundColor: "whitesmoke",
            }}
            pagination={paginationFactory({ sizePerPage: 5 })}
          >
            <thead>
              <tr style={{ border: "solid black 1px" }}>
                <th>Ime</th>
                <th>Prezime</th>
                <th>Email adresa</th>
                <th>Datum rođenja</th>
                <th>Datum upisa</th>
                <th>Obriši</th>
                <th>Članarine</th>
              </tr>
            </thead>
            <tbody>
              {this.state.members.map((m) => (
                <tr key={m.email}>
                  {Object.values(m).map((val) => (
                    <td>{val}</td>
                  ))}
                  <td key="button">
                    {" "}
                    <Button onClick={this.deleteMember.bind(this, m.email)}>
                      <DeleteIcon />
                    </Button>
                  </td>
                  <td key="info">
                    <Link to={"/members/id/" + m.email}>
                      <InfoIcon />
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>

        <div
          style={{
            margin: "auto",
            backgroundColor: "whitesmoke",
            marginTop: "3rem",
            marginBottom: "1em",
            border: "2px double #830981",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "col",
            width: "20%",
          }}
        >
          <Link
            to="/members/add"
            style={{
              display: "flex",
              justifyContent: "center",
              textDecoration: "none",
              margin: "auto",
              alignItems: "center",
              fontSize: "1.5em",
            }}
          >
            Dodaj novog člana! <PersonAddIcon />
          </Link>
        </div>
      </div>
    );
  }
}
export default Members;
