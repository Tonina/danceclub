import React, { Component } from "react";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import { Link } from "react-router-dom";

class Membership extends Component {
  state = {
    memberships: [],
    paid: [],
    notpaid: [],
    f: false,
  };

  func() {
    let paidArr = [];
    let notpaidArr = [];

    this.state.memberships.map((m) => {
      if (m.paid === true) paidArr.push(m);
      else notpaidArr.push(m);
    });

    this.state.paid = paidArr;
    this.state.notpaid = notpaidArr;

    let max = 0;

    if (this.state.notpaid.length === 0) {
      for (let i = 0; i < this.state.paid.length; i++) {
        if (this.state.paid[i].idmjesec > max)
          max = this.state.paid[i].idmjesec;
      }
    }

    console.log("max mjesec:");
    console.log(max);

    var d = new Date();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    let idMjesec = "";
    idMjesec += year;
    if (month < 10) idMjesec += 0;
    idMjesec += month;

    console.log("mjesec:");
    console.log(idMjesec);
    console.log(idMjesec <= max);
    if (idMjesec < max) this.state.f = true;
    else this.state.f = false;

    console.log(this.state.f);
  }

  componentDidMount() {
    fetch("/membership/id/" + localStorage.getItem("id"))
      .then((res) => res.json())
      .then((memberships) =>
        this.setState({ memberships }, () => console.log(memberships))
      );
  }

  render() {
    this.func();
    console.log(this.state.paid);
    console.log(this.state.notpaid);
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          height: "50%",
          flexDirection: "column",
          margin: "auto",
          backgroundColor: "whitesmoke",
          width: "30%",
          marginBottom: "1rem",
          marginTop: "5em",
          border: "solid black",
        }}
      >
        <h3
          style={{
            display: "flex",
            fontSize: "2.5vw",
            justifyContent: "center",
            opacity: 0.9,
            color: "#8d2060",
          }}
        >
          Moje članarine:
        </h3>

        <div
          style={{
            border: "solid black",
            margin: "auto",
            marginTop: "1em",
            marginBottom: "1em",
            width: "50%",
            backgroundColor: "#3CB371",
            borderRadius: "25px",
          }}
        >
          <strong
            style={{ display: "flex", alignItems: "center", flexWrap: "wrap" }}
          >
            <CheckIcon />
            PLAĆENO:
          </strong>
          {this.state.paid.map((c) => {
            return (
              <div style={{ display: "flex" }}>
                {c.nazivmjesec} : {c.cijena} kn
              </div>
            );
          })}
        </div>
        <div
          style={{
            border: "solid black",
            margin: "auto",
            marginTop: "1em",
            marginBottom: "1em",
            width: "50%",
            backgroundColor: "#FF6347",
            borderRadius: "25px",
          }}
        >
          <strong
            style={{ display: "flex", alignItems: "center", flexWrap: "wrap" }}
          >
            {" "}
            <ClearIcon /> NEPLAĆENO:
          </strong>
          {this.state.notpaid.map((c) => {
            return (
              <div style={{ display: "flex" }}>
                {c.nazivmjesec} : {c.cijena} kn
              </div>
            );
          })}
        </div>
        {this.state.notpaid.length === 0 && this.state.f === false && (
          <div
            style={{
              borderRadius: "25px",
              fontSize: "1em",
              justifyContent: "center",
              alignItems: "center",
              border: "solid black",
              margin: "auto",
              marginTop: "1em",
              marginBottom: "1em",
              width: "70%",
              padding: "0.5em",
            }}
          >
            {" "}
            S obzirom da su sve članarine plaćene,{" "}
            <Link
              to="/membership/dance"
              style={{
                justifyContent: "center",
                textDecoration: "none",
                margin: "auto",
                alignItems: "center",
              }}
            >
              {" "}
              odite izabrati plesove za idući mjesec.
            </Link>
          </div>
        )}

        {this.state.notpaid.length === 0 && this.state.f === true && (
          <div
            style={{
              borderRadius: "25px",
              fontSize: "1em",
              justifyContent: "center",
              alignItems: "center",
              border: "solid black",
              margin: "auto",
              marginTop: "1em",
              marginBottom: "1em",
              width: "70%",
              padding: "0.5em",
            }}
          >
            {" "}
            Sve članarine su plaćene, čak i jedna unaprijed.{" "}
            <p>Vratite se idući mjesec izbrati plesove za iduće mjesece!</p>
          </div>
        )}
      </div>
    );
  }
}
export default Membership;
