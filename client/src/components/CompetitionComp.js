import React, { Component } from "react";
import CompetitionCard from "./CompetitionCard";

class CategoryComp extends Component {
  state = {
    competitionList: [],
  };

  componentDidMount() {
    fetch("/competitions")
      .then((res) => res.json())
      .then((competitionList) =>
        this.setState({ competitionList }, () => console.log(competitionList))
      );
  }

  render() {
    return (
      <div>
        {this.state.competitionList.map((competition) => {
          return (
            <CompetitionCard
              competitionId={competition.idnatjecanje}
              competitionName={competition.nazivnat}
              mjesto={competition.nazivmjesto}
              drzava={competition.nazivdrzava}
              key={competition.id}
              date={competition.datumnat}
            />
          );
        })}
      </div>
    );
  }
}

export default CategoryComp;
