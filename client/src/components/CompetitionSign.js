import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./css/CompetitionSign.css";

class CompetitionSign extends Component {
  render() {
    return (
      <div>
        <header className="wrap">
          Pogledajte naše uspjehe na{" "}
          <Link to="/competitions">raznim natjecanjima</Link>{" "}
        </header>
      </div>
    );
  }
}

export default CompetitionSign;
