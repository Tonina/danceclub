import React, { Component } from "react";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";

export class TrainingParticipants extends Component {
  state = {
    m: [],
  };

  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleParticipants } = this.props;

    let help = [];
    let date =
      values.trainingDate.substring(0, 4) + values.trainingDate.substring(5, 7);

    for (let i = 0; i < values.data.members.length; i++) {
      if (
        values.data.members[i].idmjesec == date &&
        values.data.members[i].idstil == values.danceSelected.value
      ) {
        help.push(values.data.members[i]);
      }
    }
    this.state.m = help;
    console.log(this.state.m);

    return (
      <div>
        {" "}
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
          }}
        >
          Odaberite tko je sudjelovao na treningu:
        </h2>
        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
                padding: "1em",
              }}
            >
              <div
                style={{
                  fontSize: "1.5em",
                  margin: "auto",
                  marginBottom: "1em",
                }}
              >
                {this.state.m.map((m1) => {
                  return (
                    <Form.Group>
                      <Form.Check
                        type="checkbox"
                        label={m1.osoba}
                        defaultChecked={m1.hasparticipated}
                        onChange={handleParticipants({ m1 })}
                      />
                    </Form.Group>
                  );
                })}
                {this.state.m.length === 0 && (
                  <div
                    style={{
                      margin: "auto",
                      backgroundColor: "whitesmoke",
                      width: "100%",
                      marginBottom: "3rem",
                      border: "2px double #830981",
                      borderRadius: "25px",
                      display: "flex",
                      flexDirection: "col",
                      padding: "0.5em",
                    }}
                  >
                    Nitko nije upisan na ovaj stil za mjesec odabranog za ovaj
                    trening, vratite se i promijenite stil ili odustanite od
                    unosa!
                  </div>
                )}
              </div>

              <Button color="primary" variant="contained" onClick={this.back}>
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                style={{ marginLeft: "1em" }}
                onClick={this.continue}
                disabled={this.state.m.length === 0}
              >
                Nastavi
              </Button>
              <Link to="/training">
                <Button variant="contained" style={{ marginLeft: "1em" }}>
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default TrainingParticipants;
