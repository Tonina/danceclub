import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Select from "react-select";
import InputLabel from "@material-ui/core/InputLabel";

export class TrainingFormDetails extends Component {
  state = {
    options: [],
    value: [],
  };

  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleDance, handleChange, handleHall } = this.props;
    console.log(values.dance);
    return (
      <div>
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
            textAlign: "center",
          }}
        >
          Podaci o treningu:
        </h2>

        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <AppBar title="Unesite podatke o treningu" />
              <InputLabel id="label" style={{ marginTop: "0.5em" }}>
                Datum treninga*
              </InputLabel>
              <TextField
                required
                type="date"
                onChange={handleChange("trainingDate")}
                margin="normal"
                defaultValue={values.trainingDate}
                fullWidth
              />
              <br />
              <InputLabel id="label" style={{ marginTop: "0.5em" }}>
                Sat početka treninga*
              </InputLabel>
              <TextField
                required
                type="time"
                label=""
                onChange={handleChange("start")}
                defaultValue={values.start}
                margin="normal"
                fullWidth
              />
              <br />
              <InputLabel id="label" style={{ marginTop: "0.5em" }}>
                Sat završetka treninga*
              </InputLabel>
              <TextField
                required
                type="time"
                label=""
                onChange={handleChange("end")}
                defaultValue={values.end}
                margin="normal"
                fullWidth
              />
              <br />

              <InputLabel id="label">
                Plesni stil odrađen na treningu*
              </InputLabel>
              <Select
                defaultValue={values.danceSelected}
                options={values.data.dances}
                onChange={handleDance}
              ></Select>
              <br />
              <InputLabel id="label">
                Odaberite kapacitet dvorane u kojoj ste bili*
              </InputLabel>
              <Select
                defaultValue={values.hallSelected}
                options={values.data.halls}
                onChange={handleHall}
              ></Select>
              <br />
              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                style={{ marginBottom: "1em" }}
              >
                Nastavi
              </Button>
              <Link to="/training">
                <Button
                  variant="contained"
                  style={{
                    marginLeft: "1em",
                    marginBottom: "1em",
                  }}
                >
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default TrainingFormDetails;
