import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { List, ListItem, ListItemText } from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";

export class ConfirmTraining extends Component {
  state = {
    disabled: false,
    participants: [],
  };

  postMethod() {
    const { values } = this.props;
    console.log(values);
    const data = {
      id: localStorage.getItem("id"),
      date: values.trainingDate,
      start: values.start,
      end: values.end,
      idHall: values.hallSelected.value,
      idStyle: values.danceSelected.value,
      participants: this.state.participants,
    };
    console.log(data);
    fetch("/training/add", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "localhost:5000",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.err === true) {
          console.log(data.errmessage);
          alert("Greška pri dodavanju treninga!\n" + data.errmessage.detail);
          this.props.handleError().call();
          console.log("usao u err");
          console.log(values.er);
          window.location.replace("/training");
        } else {
          alert("Novi trening dodan!");
          window.location.replace("/training");
        }
      })
      .catch((error) => {
        alert("Greška pri dodavanju treninga!");
        console.error("Error:", error);
        this.props.handleError().call();
        window.location.replace("/training");
      })
      .catch(function (err) {
        alert("Greška pri dodavanju treninga!");
        console.error("Error:", err);
        this.props.handleError().call();
        window.location.replace("/training");
      });
  }
  continue = (e) => {
    e.preventDefault();
    this.postMethod();
    //this.props.nextStep();
    this.render();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values } = this.props;
    console.log(this.state.disabled);

    let e = [];
    values.data.members.map((d) => {
      console.log(d);
      if (d.hasparticipated === true) {
        e.push(d);
      }
    });
    this.state.participants = e;
    console.log(this.state.participants);

    return (
      <div>
        <MuiThemeProvider>
          <>
            <div
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <h2
                style={{
                  margin: "auto",
                  marginTop: "3em",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                Podaci o novom treningu:
              </h2>
              <p
                style={{ margin: "auto", textAlign: "center", fontSize: "1em" }}
              >
                Ako je potvrdi gumb onemogućen, vrati se i popuni sva
                nepopunjena polja.
              </p>
              <AppBar title="Potvrdi trening" />
              <List
                style={{
                  margin: "auto",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                <ListItem>
                  <ListItemText
                    primary="Plesni stil"
                    secondary={values.dance}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Datum treninga"
                    secondary={values.trainingDate}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Sat početka treninga"
                    secondary={values.start}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Sat završetka treninga"
                    secondary={values.end}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Kapacitet dvorane"
                    secondary={values.hallSelected.label}
                  />
                </ListItem>
                <ListItem>
                  <ul>
                    <ListItemText primary="Članovi koji su sudjelovali:" />
                    {this.state.participants.map((d) => {
                      return <ListItemText secondary={d.osoba} />;
                    })}
                  </ul>
                </ListItem>
              </List>
              <br />

              <Button
                color="primary"
                variant="contained"
                onClick={this.back}
                style={{ marginBottom: "1em" }}
              >
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                style={{ marginLeft: "1em", marginBottom: "1em" }}
                disabled={
                  values.trainingDate === "" ||
                  values.start === "" ||
                  values.end === "" ||
                  values.dance === "" ||
                  values.dancinghall === ""
                }
              >
                Potvrdi i dodaj trening
              </Button>
              <Button
                variant="contained"
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                <Link to="/training"> Odustani</Link>
              </Button>
            </div>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default ConfirmTraining;
