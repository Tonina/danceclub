import React from "react";
import { Card } from "react-bootstrap";
import DirectionsRunIcon from "@material-ui/icons/DirectionsRun";

function TrainingCard(props) {
  return (
    <div
      style={{
        display: "inline-block",
        margin: "1em",
      }}
    >
      <Card
        className="text-center"
        style={{
          border: "solid",
          borderColor: "#C71585",
          color: "black",
        }}
      >
        <Card.Header
          style={{
            display: "flex",
            justifyContent: "center",
            fontFamily: "Roboto",
            fontSize: "1.5vw",
            borderBottom: "solid",
            borderColor: "black",
            textDecoration: "none",
            backgroundColor: "pink",
          }}
        >
          {" "}
          <Card.Link
            href={"/training/id/" + props.trainingId}
            style={{
              textDecoration: "none",
              display: "flex",
              alignItems: "center",
              flexWrap: "wrap",
            }}
          >
            Vidi više o ovom treningu! <DirectionsRunIcon />
          </Card.Link>
        </Card.Header>
        <Card.Body>
          <Card.Text
            style={{
              fontSize: "1.2em",
              justifyContent: "center",
              alignItems: "center",
              margin: "0.5em",
            }}
          >
            {props.name} {props.surname} je održao/la trening na datum <br />
            {props.date} od {props.start} do {props.end} sati.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <div
            style={{
              fontSize: "1.2vw",
              marginTop: "1em",
              marginBottom: "1em",
            }}
          >
            {" "}
          </div>
        </Card.Footer>
      </Card>
    </div>
  );
}

export default TrainingCard;
