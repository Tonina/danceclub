import React, { Component } from "react";
import HomeIcon from "@material-ui/icons/Home";
import LockIcon from "@material-ui/icons/Lock";
import PersonIcon from "@material-ui/icons/Person";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import "./css/NavBar.css";

class NavBar extends Component {
  state = {
    categoriesList: [],
  };

  logout() {
    localStorage.clear();
  }

  render() {
    const firstName = localStorage.getItem("name");
    const lastName = localStorage.getItem("surname");
    const style = {
      backgroundColor: "#8d2060",
      height: "6%",
      width: "100%",
      position: "fixed",
      top: "0",
      opacity: 0.9,
      fontSize: "1.2em",
    };

    return (
      <div style={style}>
        <ul id="nav">
          <li>
            <a
              href="/"
              style={{
                marginLeft: "1em",
                color: "white",
                textDecoration: "none",
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
              }}
            >
              Početna stranica <HomeIcon />{" "}
            </a>
          </li>
          <li>
            {localStorage.getItem("id") == null && (
              <a
                href="/login"
                style={{
                  marginRight: "1rem",
                  color: "white",
                  textDecoration: "none",
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                Prijavi se <LockIcon />
              </a>
            )}
          </li>
          <li>
            {localStorage.getItem("id") != null && (
              <a
                href="/profile"
                style={{
                  color: "white",
                  textDecoration: "none",
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                {firstName} {lastName} <PersonIcon />
              </a>
            )}
          </li>
          <li>
            {localStorage.getItem("id") != null && (
              <a
                href="/"
                onClick={() => this.logout()}
                style={{
                  color: "white",
                  textDecoration: "none",
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                }}
              >
                Odjava
                <ExitToAppIcon />
              </a>
            )}
          </li>
        </ul>
      </div>
    );
  }
}

export default NavBar;
