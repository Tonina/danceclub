import React, { useState } from "react";
import slika1 from "../images/dance1.png";
import slika2 from "../images/dance2.png";
import { Figure } from "react-bootstrap";
import "./css/SlideShow.css";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";

function SlideShow() {
  return (
    <div
      className="slideshow"
      style={{
        margin: "0",
        textAlign: "center",
      }}
    >
      <AliceCarousel
        autoPlay
        disableButtonsControls
        infinite
        autoPlayInterval="1000"
      >
        <Figure>
          <Figure.Image alt="171x180" src={slika1} className="sliderimg" />
        </Figure>
        <Figure>
          <Figure.Image src={slika2} alt="druga" className="sliderimg" />
        </Figure>
      </AliceCarousel>
    </div>
  );
}

export default SlideShow;
