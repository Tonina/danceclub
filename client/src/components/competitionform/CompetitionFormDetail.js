import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Select from "react-select";
import InputLabel from "@material-ui/core/InputLabel";

export class CompetitionFormDetails extends Component {
  state = {
    options: [],
    value: [],
  };

  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleChange, handleCity } = this.props;
    return (
      <div>
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
            textAlign: "center",
          }}
        >
          Podaci o natjecanju:
        </h2>
        <p style={{ margin: "auto", textAlign: "center", fontSize: "1em" }}>
          Nastavi gumb će biti onemogućen sve dok naziv natjecanja nije ispunjen
          ili datum nije odabran.
        </p>
        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <AppBar title="Unesite podatke o natjecanju" />
              <TextField
                required
                placeholder="Unesite naziv natjecanja"
                label="Naziv natjecanja"
                onChange={handleChange("competitionName")}
                defaultValue={values.competitionName}
                margin="normal"
                fullWidth
              />
              <br />
              <br />
              <InputLabel id="label">Datum natjecanja*</InputLabel>
              <TextField
                required
                type="date"
                onChange={handleChange("competitionDate")}
                defaultValue={values.competitionDate}
                margin="normal"
                fullWidth
              />
              <br />
              <br />
              <InputLabel id="label">Grad*</InputLabel>
              <Select
                defaultValue={values.city}
                options={values.data.cities}
                onChange={handleCity}
              ></Select>
              <br />

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                disabled={
                  values.competitionName.length < 1 ||
                  values.competitionDate === ""
                }
                style={{ marginBottom: "1em" }}
              >
                Nastavi
              </Button>
              <Link to="/competitions">
                <Button
                  variant="contained"
                  style={{
                    marginLeft: "1em",
                    marginBottom: "1em",
                  }}
                >
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default CompetitionFormDetails;
