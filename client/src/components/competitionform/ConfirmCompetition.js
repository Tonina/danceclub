import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { List, ListItem, ListItemText } from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";

export class Confirm extends Component {
  state = {
    disabled: false,
  };

  postMethod() {
    const { values } = this.props;
    console.log(values);
    const data = {
      idmjesto: values.city.value,
      competitionDate: values.competitionDate,
      competitionName: values.competitionName,
      competitors: values.c,
    };
    console.log(data);
    fetch("/competitions/add", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "localhost:5000",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.err === true) {
          console.log(data.errmessage);
          alert(
            "Greška prilikom dodavanja natjecanja!\n" + data.errmessage.detail
          );
          this.props.handleError().call();
          console.log("usao u err");
          console.log(values.er);
          window.location.replace("/competitions");
        } else {
          alert("Novo natjecanje dodano!");
          window.location.replace("/competitions");
        }
      })
      .catch((error) => {
        alert("Greška prilikom dodavanja natjecanja!");
        console.error("Error:", error);
        this.props.handleError().call();
        window.location.replace("/competitions");
      })
      .catch(function (err) {
        alert("Greška prilikom dodavanja natjecanja!");
        console.error("Error:", err);
        this.props.handleError().call();
        window.location.replace("/competitions");
      });
  }
  continue = (e) => {
    e.preventDefault();
    this.postMethod();
    //this.props.nextStep();
    this.render();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values } = this.props;
    values.c.map((comp) => {
      if (comp.plasman === "" || comp.idStil === "") {
        this.state.disabled = true;
      }
    });
    console.log(values.city.label);
    if (values.city.label === undefined) this.state.disabled = true;
    console.log(this.state.disabled);

    return (
      <div>
        <MuiThemeProvider>
          <>
            <div
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <h2
                style={{
                  margin: "auto",
                  marginTop: "3em",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                Podaci o novom natjecanju:
              </h2>
              <p
                style={{ margin: "auto", textAlign: "center", fontSize: "1em" }}
              >
                Ako je potvrdi gumb onemogućen, vrati se i ispuni neispunjena
                polja.
              </p>
              <AppBar title="Potvrdi podatke" />
              <List
                style={{
                  margin: "auto",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                <ListItem>
                  <ListItemText
                    primary="Naziv natjecanja"
                    secondary={values.competitionName}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Datum natjecanja"
                    secondary={values.competitionDate}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Grad:" secondary={values.city.label} />
                </ListItem>
                <ListItem>
                  <ul>
                    <ListItemText primary="Natjecatelji:" />
                    {values.c.map((comp) => {
                      return (
                        <p style={{ display: "flex" }}>
                          <ListItemText
                            style={{
                              flexDirection: "column",
                              marginRight: "0.5rem",
                            }}
                            secondary={comp.osoba}
                          />
                          <ListItemText
                            style={{
                              flexDirection: "column",
                              marginRight: "0.5rem",
                            }}
                            secondary={"je"}
                          />

                          <ListItemText
                            style={{
                              flexDirection: "column",
                              marginRight: "0.5rem",
                            }}
                            secondary={"osvojio/la"}
                          />
                          <ListItemText
                            style={{
                              flexDirection: "column",
                            }}
                            secondary={comp.plasman}
                          />
                          <ListItemText
                            style={{
                              flexDirection: "column",
                              marginRight: "0.5rem",
                            }}
                            secondary={".mjesto"}
                          />
                          <ListItemText
                            style={{
                              flexDirection: "column",
                              marginRight: "0.5rem",
                            }}
                            secondary={"u"}
                          />
                          <ListItemText
                            style={{
                              flexDirection: "column",
                            }}
                            secondary={comp.value.label}
                          />
                          <ListItemText
                            style={{
                              flexDirection: "column",
                            }}
                            secondary={"-u"}
                          />
                        </p>
                      );
                    })}
                  </ul>
                </ListItem>
              </List>
              <br />

              <Button
                color="primary"
                variant="contained"
                onClick={this.back}
                style={{ marginBottom: "1em" }}
              >
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                style={{ marginLeft: "1em", marginBottom: "1em" }}
                disabled={this.state.disabled}
              >
                Potvrdi i dodaj <br /> novo natjecanje
              </Button>
              <Button
                variant="contained"
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                <Link to="/competitions"> Odustani</Link>
              </Button>
            </div>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Confirm;
