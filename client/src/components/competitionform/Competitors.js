import React, { Component } from "react";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";

export class Competitors extends Component {
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  options = () => {
    const { values } = this.props;
    let o = [];
    values.dances.map((d) => {
      let value = d.idstil;
      let label = d.nazivstil;
      let data = { value: value, label: label };
      o.push(data);
    });
    return o;
  };

  render() {
    const { values, handleCompetitors } = this.props;
    console.log(values);
    console.log(values.c);

    return (
      <div>
        {" "}
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
          }}
        >
          Odaberi natjecatelje:
        </h2>
        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
                padding: "1em",
              }}
            >
              <div
                style={{
                  fontSize: "1.5em",
                  margin: "auto",
                  marginBottom: "1em",
                }}
              >
                {values.data.members.map((m) => {
                  return (
                    <Form.Group>
                      <Form.Check
                        type="checkbox"
                        label={m.osoba}
                        defaultChecked={m.iscompetitor}
                        onChange={handleCompetitors({ m })}
                      />
                    </Form.Group>
                  );
                })}
              </div>

              <Button color="primary" variant="contained" onClick={this.back}>
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                style={{ marginLeft: "1em" }}
                onClick={this.continue}
              >
                Nastavi
              </Button>
              <Link to="/competitions">
                <Button variant="contained" style={{ marginLeft: "1em" }}>
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Competitors;
