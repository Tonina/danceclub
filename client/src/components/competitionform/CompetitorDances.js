import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Select from "react-select";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";

export class Confirm extends Component {
  state = {
    loading: true,
  };

  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
    this.render();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  options = () => {
    const { values } = this.props;
    let o = [];
    values.dances.map((d) => {
      let value = d.idstil;
      let label = d.nazivstil;
      let data = { value: value, label: label };
      o.push(data);
    });
    return o;
  };

  render() {
    const { values, handleCompetitionStyle, handleCompetitionPlace } =
      this.props;
    return (
      <div>
        <MuiThemeProvider>
          <>
            <div
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <h2
                style={{
                  margin: "auto",
                  marginTop: "3em",
                  width: "100%",
                }}
              >
                Odaberite u kojem plesnom stilu je koji član osvojio koje
                mjesto:
              </h2>
              <AppBar title="" />

              {values.c.map((comp) => (
                <div
                  key="person"
                  style={{
                    marginBottom: "1em",
                    border: "solid black",
                    padding: "1em",
                  }}
                >
                  {" "}
                  <InputLabel id="label" style={{ fontSize: "1.5em" }}>
                    {comp.osoba}
                  </InputLabel>
                  <Select
                    defaultValue={comp.value}
                    options={this.options()}
                    onChange={handleCompetitionStyle({ comp })}
                  ></Select>
                  <TextField
                    required
                    type="number"
                    placeholder="Unesite plasman"
                    label="PLASMAN"
                    onChange={handleCompetitionPlace({ comp })}
                    defaultValue={comp.plasman}
                    margin="normal"
                    fullWidth
                  />
                  <br />
                </div>
              ))}

              <br />

              <Button
                color="primary"
                variant="contained"
                onClick={this.back}
                style={{ marginBottom: "1em" }}
              >
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                Nastavi
              </Button>
              <Button
                variant="contained"
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                <Link to="/competitions"> Odustani</Link>
              </Button>
            </div>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Confirm;
