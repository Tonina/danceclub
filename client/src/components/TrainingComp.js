import React, { Component } from "react";
import TrainingCard from "./TrainingCard";

class TrainingComp extends Component {
  state = {
    trainingList: [],
  };

  componentDidMount() {
    fetch("/training")
      .then((res) => res.json())
      .then((trainingList) =>
        this.setState({ trainingList }, () => console.log(trainingList))
      );
  }

  render() {
    return (
      <div>
        {this.state.trainingList.map((training) => {
          return (
            <TrainingCard
              trainingId={training.idtrening}
              date={training.datumtrening}
              end={training.satkraj}
              start={training.satpocetak}
              dancestyle={training.nazivstil}
              capacity={training.kapacitet}
              name={training.ime}
              surname={training.prezime}
            />
          );
        })}
      </div>
    );
  }
}

export default TrainingComp;
