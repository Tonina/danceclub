import React from "react";
import { Card } from "react-bootstrap";
import EmojiEventsIcon from "@material-ui/icons/EmojiEvents";

function CompetitionCard(props) {
  return (
    <div
      style={{
        display: "inline-block",
        margin: "1em",
      }}
    >
      <Card
        className="text-center"
        style={{
          border: "solid",
          borderColor: "#C71585",
          color: "black",
        }}
      >
        <Card.Header
          style={{
            display: "flex",
            justifyContent: "center",
            fontFamily: "Roboto",
            fontSize: "1.5em",
            borderBottom: "solid",
            borderColor: "black",
            textDecoration: "none",
            backgroundColor: "pink",
          }}
        >
          {" "}
          {props.competitionName}
          <EmojiEventsIcon />
        </Card.Header>
        <Card.Body>
          <Card.Text></Card.Text>
        </Card.Body>
        <Card.Footer>
          <div
            style={{
              fontSize: "1.2em",
              marginTop: "1em",
              marginBottom: "1em",
              justifyContent: "center",
              display: "flex",
            }}
          >
            {" "}
            Natjecanje održano u mjestu {props.mjesto},<br /> {props.drzava} na
            datum {props.date}
          </div>
        </Card.Footer>
        <Card.Link
          href={"/competitions/id/" + props.competitionId}
          style={{
            fontSize: "1.2em",
            justifyContent: "center",
            display: "flex",
          }}
        >
          Vidi više o ovom natjecanju!
        </Card.Link>
      </Card>
    </div>
  );
}

export default CompetitionCard;
