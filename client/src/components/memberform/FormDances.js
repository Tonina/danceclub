import React, { Component } from "react";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";

export class FormDances extends Component {
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleDances } = this.props;
    return (
      <div>
        {" "}
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
          }}
        >
          Odaberi plesove na koje se upisuje:
        </h2>
        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
                padding: "1em",
              }}
            >
              <div
                style={{
                  fontSize: "1.5em",
                  margin: "auto",
                  marginBottom: "1em",
                }}
              >
                {values.dances.map((d) => {
                  return (
                    <Form.Check
                      type="checkbox"
                      label={d.nazivstil}
                      onChange={handleDances({ d })}
                      defaultChecked={d.isdance}
                    />
                  );
                })}
              </div>

              <Button color="primary" variant="contained" onClick={this.back}>
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                style={{ marginLeft: "1em" }}
                onClick={this.continue}
              >
                Nastavi
              </Button>
              <Link to="/members">
                <Button variant="contained" style={{ marginLeft: "1em" }}>
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default FormDances;
