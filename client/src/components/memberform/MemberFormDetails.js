import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import InputLabel from "@material-ui/core/InputLabel";

export class MemberFormDetails extends Component {
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (
      <div>
        <h2
          style={{
            margin: "auto",
            marginTop: "3em",
            width: "30%",
            textAlign: "center",
          }}
        >
          Osobni podaci člana kluba:
        </h2>
        <p style={{ margin: "auto", textAlign: "center", fontSize: "1em" }}>
          Nastavi gumb će biti onemogućen sve dok obavezna polja nisu ispunjena{" "}
          <br />
          (ili je duljina OIB-a različita od 11).
        </p>
        <MuiThemeProvider>
          <>
            <Form
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <AppBar title="Unesi podatke o članu kluba" />
              <TextField
                required
                placeholder="Unesi ime"
                label="Ime"
                onChange={handleChange("firstName")}
                defaultValue={values.firstName}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                placeholder="Unesi prezime"
                label="Prezime"
                onChange={handleChange("lastName")}
                defaultValue={values.lastName}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                placeholder="Unesi email adresu"
                label="Email adresa"
                onChange={handleChange("email")}
                defaultValue={values.email}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                type="number"
                placeholder="Unesi OIB"
                label="OIB"
                onChange={handleChange("oib")}
                defaultValue={values.oib}
                margin="normal"
                fullWidth
              />
              <br />
              <br />
              <InputLabel id="label">Datum rođenja*</InputLabel>
              <TextField
                required
                type="date"
                onChange={handleChange("datebirth")}
                defaultValue={values.datebirth}
                margin="normal"
                fullWidth
              />
              <br />
              <TextField
                required
                type="password"
                secureTextEntry={true}
                placeholder="Unesi lozinku"
                label="Lozinka"
                onChange={handleChange("password")}
                defaultValue={values.password}
                margin="normal"
                fullWidth
              />
              <br />

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                disabled={
                  values.email.length < 1 ||
                  values.oib.length !== 11 ||
                  values.firstName.length < 1 ||
                  values.lastName.length < 1 ||
                  values.password.length < 1 ||
                  values.datebirth === ""
                }
                style={{ marginBottom: "1em" }}
              >
                Nastavi
              </Button>
              <Link to="/members">
                <Button
                  variant="contained"
                  style={{ marginLeft: "1em", marginBottom: "1em" }}
                >
                  Odustani
                </Button>
              </Link>
            </Form>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default MemberFormDetails;
