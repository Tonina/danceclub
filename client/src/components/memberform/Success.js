import React, { Component } from "react";
import { Link } from "react-router-dom";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import GroupIcon from "@material-ui/icons/Group";

export class Success extends Component {
  state = {
    enrolled: [],
    loading: true,
  };
  continue = (e) => {
    e.preventDefault();
    // PROCESS FORM //
    this.props.nextStep();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };
  refresh = (e) => {
    this.state.loading = false;
    this.render();
  };

  render() {
    const { values } = this.props;
    console.log("u render");
    console.log(values.isSuccessful);
    console.log("loading:");
    console.log(this.state.loading);
    return (
      <div
        style={{
          margin: "auto",
          backgroundColor: "whitesmoke",
          marginTop: "5em",
          marginBottom: "1em",
          border: "2px double #830981",
          borderRadius: "25px",
          display: "flex",
          flexDirection: "col",
          width: "30%",
        }}
      >
        <Link
          to="/members"
          style={{
            display: "flex",
            justifyContent: "center",
            textDecoration: "none",
            margin: "auto",
            alignItems: "center",
            fontSize: "1.5em",
          }}
        >
          <KeyboardBackspaceIcon /> <GroupIcon />
          Vrati se na popis članova kluba!
        </Link>
      </div>
    );
  }
}

export default Success;
