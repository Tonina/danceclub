import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { List, ListItem, ListItemText } from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

export class Confirm extends Component {
  state = {
    enrolled: [],
    done: false,
    loading: true,
  };

  postMethod() {
    const { values } = this.props;
    console.log(this.state.enrolled);
    const data = {
      name: values.firstName,
      surname: values.lastName,
      email: values.email,
      oib: values.oib,
      pass: values.password,
      datebirth: values.datebirth,
      dances: this.state.enrolled,
    };
    fetch("/members/add", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "localhost:5000",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.err === true) {
          console.log(data.errmessage);
          alert("Greška pri dodavanju člana!\n" + data.errmessage.detail);
          this.props.handleError().call();
          console.log("usao u err");
          console.log(values.er);
        } else {
          alert("Novi član dodan!");
        }
      })
      .catch((error) => {
        alert("Greška pri dodavanju člana!");
        console.error("Error:", error);
        this.props.handleError().call();
      })
      .catch(function (err) {
        alert("Greška pri dodavanju člana!");
        console.error("Error:", err);
        this.props.handleError().call();
      });
  }
  continue = (e) => {
    e.preventDefault();
    this.state.done = true;
    this.postMethod();
    this.props.nextStep();
    this.render();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values } = this.props;
    let e = [];
    values.dances.map((d) => {
      console.log(d);
      if (d.isdance === true) {
        e.push(d);
      }
    });
    this.state.enrolled = e;
    console.log(this.state.enrolled);
    console.log(this.state.done);

    return (
      <div>
        <MuiThemeProvider>
          <>
            <div
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <h2
                style={{
                  margin: "auto",
                  marginTop: "3em",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                Uneseni podaci za novog člana:
              </h2>
              <AppBar title="Potvrdi podatke" />
              <List
                style={{
                  margin: "auto",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                <ListItem>
                  <ListItemText primary="Ime" secondary={values.firstName} />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Prezime" secondary={values.lastName} />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Email adresa"
                    secondary={values.email}
                  />
                </ListItem>
                <ListItem>
                  <ListItemText primary="OIB" secondary={values.oib} />
                </ListItem>
                <ListItem>
                  <ListItemText
                    primary="Datum rođenja"
                    secondary={values.datebirth}
                  />
                </ListItem>
                <ListItem>
                  <ul>
                    <ListItemText primary="Plesovi na koje će biti upisan:" />
                    {this.state.enrolled.map((d) => {
                      return <ListItemText secondary={d.nazivstil} />;
                    })}
                  </ul>
                </ListItem>
              </List>
              <br />

              <Button
                color="primary"
                variant="contained"
                onClick={this.back}
                style={{ marginBottom: "1em" }}
              >
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                Potvrdi i dodaj člana
              </Button>
              <Button
                variant="contained"
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                <Link to="/members"> Odustani</Link>
              </Button>
            </div>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Confirm;
