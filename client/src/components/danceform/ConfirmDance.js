import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { List, ListItem, ListItemText } from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

export class Confirm extends Component {
  state = {
    enrolled: [],
    done: false,
    loading: true,
  };

  postMethod() {
    const { values } = this.props;
    console.log(this.state.enrolled);
    const data = {
      id: localStorage.getItem("id"),
      dances: this.state.enrolled,
    };
    fetch("/membership/dances", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "localhost:5000",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.err === true) {
          console.log(data.errmessage);
          alert("Greška prilikom upisivanja!\n" + data.errmessage.detail);
          this.props.handleError().call();
          console.log("usao u err");
          console.log(values.er);
        } else {
          alert("Upisan!");
        }
      })
      .catch((error) => {
        alert("Greška prilikom upisivanja!\n");
        console.error("Error:", error);
        this.props.handleError().call();
      })
      .catch(function (err) {
        alert("Greška prilikom upisivanja!\n");
        console.error("Error:", err);
        this.props.handleError().call();
      });
  }
  continue = (e) => {
    e.preventDefault();
    this.state.done = true;
    this.postMethod();
    this.props.nextStep();
    this.render();
  };

  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values } = this.props;
    let e = [];
    values.dances.map((d) => {
      console.log(d);
      if (d.isdance === true) {
        e.push(d);
      }
    });
    this.state.enrolled = e;
    console.log(this.state.enrolled);
    console.log(this.state.done);

    return (
      <div>
        <MuiThemeProvider>
          <>
            <div
              style={{
                margin: "auto",
                width: "30%",
              }}
            >
              <h2
                style={{
                  margin: "auto",
                  marginTop: "3em",
                  width: "100%",
                }}
              >
                Odabrani plesovi:
              </h2>
              <AppBar title="Potvrdi odabrane plesove" />
              <List
                style={{
                  margin: "auto",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                <ListItem>
                  <ul>
                    <ListItemText primary="Plesovi na koje ćete biti upisani:" />
                    {this.state.enrolled.map((d) => {
                      return <ListItemText secondary={d.nazivstil} />;
                    })}
                  </ul>
                </ListItem>
              </List>
              <br />

              <Button
                color="primary"
                variant="contained"
                onClick={this.back}
                style={{ marginBottom: "1em" }}
              >
                Natrag
              </Button>

              <Button
                color="secondary"
                variant="contained"
                onClick={this.continue}
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                Potvrdi i upiši
              </Button>
              <Button
                variant="contained"
                style={{ marginLeft: "1em", marginBottom: "1em" }}
              >
                <Link to="/membership"> Odustani</Link>
              </Button>
            </div>
          </>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Confirm;
