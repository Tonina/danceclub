import React, { Component } from "react";

class Footer extends Component {
  constructor() {
    super();
    this.state = {
      owners: [],
    };
  }
  componentDidMount() {
    fetch("/footer")
      .then((res) => res.json())
      .then((owners) => this.setState({ owners }, () => console.log(owners)));
  }
  render() {
    return (
      <div
        style={{
          color: "white",
          background: "#8d2060",
          opacity: 0.9,
          paddingTop: "1.5em",
          position: "relative",
          bottom: "0",
          width: "100%",
          // eslint-disable-next-line
          position: "static",
          marginTop: "1em",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col">
              <h4>~Dodatne informacije o našim vlasnicima </h4>
              <ul
                style={{
                  padding: "0.5em",
                }}
              >
                <li>
                  {this.state.owners.map((o) => {
                    return (
                      <p>
                        Ime i Prezime :{o.ime} {o.prezime} - email: {o.email}
                      </p>
                    );
                  })}
                </li>
              </ul>
            </div>
          </div>
          <hr />
          <div className="row">
            <p className="col-sm">
              &copy;{new Date().getFullYear()} Tonina Tičinović | All rights
              reserved | Terms Of Service | Privacy
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
