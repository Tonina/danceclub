const express = require("express");
const db = require("./db");
const loginRoute = require("./routes/login.routes");
const profileRoute = require("./routes/profile.routes");
const competitionRoute = require("./routes/competitions.routes");
const membershipRoute = require("./routes/membership.routes");
const membersRoute = require("./routes/members.routes");
const trainersRoute = require("./routes/trainers.routes");
const trainingRoute = require("./routes/training.routes");

var cors = require("cors");

const app = express();
app.use(cors());

app.use(
  express.urlencoded({ limit: "50mb", extended: false, parameterLimit: 50000 })
);
app.use(express.json({ limit: "50mb" }));

app.get("/footer", async (req, res) => {
  const sqlOwner = "SELECT * FROM vlasnikKluba NATURAL JOIN korisnik;";
  try {
    const resultOwner = (await db.query(sqlOwner, [])).rows;
    res.json(resultOwner);
  } catch (err) {
    console.log(err);
  }
});

app.get("/dances", async (req, res) => {
  const sqlDances = "SELECT * FROM plesnistil NATURAL JOIN f;";
  try {
    const resultDances = (await db.query(sqlDances, [])).rows;
    res.json(resultDances);
  } catch (err) {
    console.log(err);
  }
});

/*app.use(
  express.urlencoded({
    extended: true,
  })
);*/
//app.use(express.json());

app.use("/login", loginRoute);
app.use("/profile", profileRoute);
app.use("/competitions", competitionRoute);
app.use("/membership", membershipRoute);
app.use("/members", membersRoute);
app.use("/trainers", trainersRoute);
app.use("/training", trainingRoute);

app.listen(5000);
