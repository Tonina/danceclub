const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "danceclub",
  password: "bazepodataka",
  port: 5432,
});

const sql_create_f = ` CREATE TABLE f (isDance bool)`;
const sql_create_clanarina = `CREATE TABLE clanarina (
    idClanarina int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    brojPlesnihStilova int NOT NULL,
    cijena int NOT NULL
)`;

const sql_create_clanarina_id_index = `CREATE UNIQUE INDEX idx_clanarinaId ON clanarina(idClanarina)`;

const sql_create_uloga = `CREATE TABLE uloga (
    idUloga int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nazivULoga varchar(50) NOT NULL
)`;
const sql_create_uloga_id_index = `CREATE UNIQUE INDEX idx_ulogaId ON uloga(idUloga)`;

const sql_create_users = `CREATE TABLE korisnik (
    idKorisnik int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    ime text NOT NULL,
    prezime text NOT NULL,
    email text UNIQUE NOT NULL,
    lozinka text NOT NULL,
    datumRodenja date NOT NULL,
    OIB numeric(11) UNIQUE, 
    idUloga int NOT NULL REFERENCES uloga(idUloga)
)`;

const sql_create_users_id_index = `CREATE UNIQUE INDEX idx_usersId ON korisnik(idKorisnik)`;

const sql_create_clanKluba = `CREATE TABLE clanKluba (
    brClanskeIskaznice int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    datumUpisa date NOT NULL,
    idKorisnik int NOT NULL UNIQUE REFERENCES korisnik(idKorisnik) ON DELETE CASCADE
)`;
const sql_create_clanKluba_id_index = `CREATE UNIQUE INDEX idx_clanKlubaId ON clanKluba(brClanskeIskaznice)`;

const sql_create_vlasnikKluba = `CREATE TABLE vlasnikKluba (
    idVlasnik int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    datumVlasnistvo date NOT NULL,
    idKorisnik int NOT NULL UNIQUE REFERENCES korisnik(idKorisnik)  ON DELETE CASCADE
)`;
const sql_create_vlasnikKluba_id_index = `CREATE UNIQUE INDEX idx_vlasnikKlubaId ON vlasnikKluba(idVlasnik)`;

const sql_create_trener = `CREATE TABLE trener (
    idTrener int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    oznakaKinFakultet boolean NOT NULL,
    brojMobitel numeric(10),
    idKorisnik int NOT NULL UNIQUE REFERENCES korisnik(idKorisnik) ON DELETE CASCADE
)`;
const sql_create_trener_id_index = `CREATE UNIQUE INDEX idx_trenerId ON trener(idTrener)`;

const sql_create_drzava = `CREATE TABLE drzava (
    idDrzava int  GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nazivDrzava varchar(50)
)`;

const sql_create_drzava_id_index = `CREATE INDEX idx_drzavaId ON drzava(idDrzava)`;

const sql_create_dvorana = `CREATE TABLE dvorana (
    idDvorana int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    kapacitet int
)`;

const sql_create_dvorana_index = `CREATE INDEX idx_dvoranaId ON dvorana(dvoranaId)`;

const sql_create_mjesec = `CREATE TABLE mjesec (
    idMjesec int PRIMARY KEY,
    nazivMjesec varchar(50)
)`;

const sql_create_mjesec_index = `CREATE INDEX idx_mjesecId ON mjesec(idMjesec)`;

const sql_create_mjesto = `CREATE TABLE mjesto (
    idMjesto int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nazivMjesto varchar(50),
    idDrzava int NOT NULL REFERENCES drzava(idDrzava)
)`;
const sql_create_mjesto_index = `CREATE INDEX idx_mjestoId ON mjesto(idMjesto)`;

const sql_create_natjecanje = `CREATE TABLE natjecanje (
    idNatjecanje int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nazivNat varchar(50) UNIQUE,
    datumNat date,
    idMjesto int NOT NULL REFERENCES mjesto(idMjesto)
)`;
const sql_create_natjecanje_index = `CREATE INDEX idx_natjecanjeId ON natjecanje(idNatjecanje)`;

const sql_create_placa = `CREATE TABLE placa (
    brClanskeIskaznice int REFERENCES clanKluba(brClanskeIskaznice) ON DELETE CASCADE,
    idMjesec int REFERENCES mjesec(idMjesec),
    idClanarina int REFERENCES clanarina(idClanarina),
    paid bool NOT NULL DEFAULT false,
    PRIMARY KEY (brClanskeIskaznice, idMjesec, idClanarina)

)`;

const sql_create_plesniStil = `CREATE TABLE plesniStil (
    idStil int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nazivStil varchar(50)
)`;
const sql_create_plesniStil_index = `CREATE INDEX idx_plesniStilId ON plesniStil(idStil)`;

const sql_create_se_natjecao_na = `CREATE TABLE se_natjecao_na (
    brClanskeIskaznice int REFERENCES clanKluba(brClanskeIskaznice) ON DELETE CASCADE,
    idStil int REFERENCES plesniStil(idStil),
    idNatjecanje int REFERENCES natjecanje(idNatjecanje),
    plasman int,
    PRIMARY KEY (brClanskeIskaznice, idNatjecanje)
)`;

const sql_create_trening = `CREATE TABLE trening (
    idTrening int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    datumTrening date NOT NULL,
    idTrener int NOT NULL REFERENCES trener(idTrener),
    idStil int NOT NULL REFERENCES plesniStil(idStil),
    satPocetak time,
    satKraj time,
    idDvorana int NOT NULL REFERENCES dvorana(idDvorana),
    CHECK (satKraj > satPocetak),
    UNIQUE (datumTrening, idDvorana)
)`;
const sql_create_trening_id_index = `CREATE UNIQUE INDEX idx_treningId ON trening(idTrening)`;

const sql_create_sudjeluje_na = `CREATE TABLE sudjeluje_na (
    brClanskeIskaznice int REFERENCES clanKluba(brClanskeIskaznice) ON DELETE CASCADE,
    idTrening int REFERENCES trening(idTrening),
    PRIMARY KEY (brClanskeIskaznice, idTrening)
)`;

const sql_create_upisuje = `CREATE TABLE upisuje (
    brClanskeIskaznice int REFERENCES clanKluba(brClanskeIskaznice) ON DELETE CASCADE,
    idStil int REFERENCES plesniStil(idStil),
    idMjesec int REFERENCES mjesec(idMjesec),
    PRIMARY KEY (brClanskeIskaznice, idStil, idMjesec)
)`;

const sql_insert_f = `INSERT INTO f (isDance) VALUES (false)`;
const sql_insert_clanarina = `INSERT INTO clanarina (
    brojPlesnihStilova, cijena)
    VALUES 
    (1, 250),
    (2, 450),
    (3, 600),
    (4, 700),
    (5, 800),
    (6, 900),
    (7, 1000);
`;

const sql_insert_uloga = `INSERT INTO uloga (nazivUloga) VALUES 
    ('vlasnik'),
    ('trener'),
    ('clan');
`;

const sql_insert_users = `INSERT INTO 
korisnik (ime, prezime, email, lozinka, datumRodenja, OIB, idUloga) 
VALUES ( 'Adminko', 'Administratović', 'null@admin', '$2b$10$Yw/9otI1HC8bkJkkaS6BeueJuUJPH1zCQm8FzD/PbjA3IE7Lxn71y',
 '01.01.1999.',12345678901, 1),
( 'Trenerko', 'Trenerkić', 'null@trener', '$2b$10$ndGdWt9Q2gVGFaveNcZz0eoC5xhHfi2560NJ930kzdlznw9lnC52O',
 '02.02.1999.',23456789012, 2),
('Članko', 'Člankić', 'null@clan', '$2b$10$JUPlWXshTTZS8s2Ycqce..HL.e.DRSXJaDSmq3mFbO7r2OWgsazme',
 '03.03.1999.',34567890123, 3),
('Članica', 'Članić', 'null@clanica', '$2b$10$knzJwTNSFaSENmeyNJ/o9.Y/IHAKSykBmkm98394higzAlMHrV8Sq',
 '05.03.2002.',34567786123, 3)`;

const sql_insert_clanKluba = `INSERT INTO clanKluba (datumUpisa, idKorisnik)
 VALUES ('02.01.2020.', 3), ('01.03.2021.' , 4)`;

const sql_insert_vlasnikKluba = `INSERT INTO vlasnikKluba (datumVlasnistvo, idKorisnik)
 VALUES ('02.01.2000.', 1)`;

const sql_insert_trener = `INSERT INTO trener (oznakaKinFakultet , brojMobitel, idKorisnik)
 VALUES (true, 1234576888, 2)`;

const sql_insert_drzava = `INSERT INTO drzava (nazivDrzava)
 VALUES ('Hrvatska'), ('Njemačka'), ('Francuska'), ('Italija')`;

const sql_insert_dvorana = `INSERT INTO dvorana (kapacitet)
 VALUES (50), (100), (20)`;

const sql_insert_mjesec = `INSERT INTO mjesec (idMjesec, nazivMjesec)
 VALUES (202101, 'siječanj 2021'), (202102, 'veljača 2021'), (202103, 'ožujak 2021'),
  (202104, 'travanj 2021'),(202105, 'svibanj 2021'), (202106, 'lipanj 2021'), (202107, 'srpanj 2021'), 
  (202108, 'kolovoz 2021'), (202109, 'rujan 2021'), (202110, 'listopad 2021'), (202111, 'studeni 2021'), (202112, 'prosinac 2021')`;

const sql_insert_mjesto = `INSERT INTO mjesto (nazivMjesto, idDrzava)
  VALUES ('Split', 1), ('Zagreb', 1), ('Berlin', 2), ('Pariz', 3), ('Rim', 4), ('Verona',4)`;

const sql_insert_natjecanje = `INSERT INTO natjecanje (nazivNat, datumNat, idMjesto)
  VALUES ('Državno prvenstvo Hrvatske', '10.09.2019.', 2), ('Svjetsko prvenstvo u breakdance-u', '20.12.2020', 4), 
  ('International competition', '23.04.2020', 6)`;

const sql_insert_placa = `INSERT INTO placa (brClanskeIskaznice, idMjesec, idClanarina, paid)
  VALUES (1, 202103, 2, true), (1, 202104, 4, true), 
  (1, 202105, 2, false), (2, 202103, 2, true), (2, 202104, 3, true), (2, 202105, 2, false)`;

const sql_insert_plesniStil = `INSERT INTO plesniStil (nazivStil)
  VALUES ('breaking'), ('hip-hop'), ('house'), ('vogue'), ('popping'), ('locking'), ('waacking')`;

const sql_insert_se_natjecao_na = `INSERT INTO se_natjecao_na (brClanskeIskaznice, idStil, idNatjecanje, plasman)
  VALUES (1, 2, 1, 3), (1, 6, 3, 1), (2, 1, 2, 5), (2, 2, 3, 4)`;

const sql_insert_trening = `INSERT INTO trening (datumTrening, idTrener, idStil, idDvorana, satPocetak, satKraj)
  VALUES ('02.03.2020', 1, 2, 1, '14:00:00', '15:30:00' ), ('04.05.2020', 1, 1, 2, '11:30:00', '12:30:00')`;

const sql_insert_sudjeluje_na = `INSERT INTO sudjeluje_na (brClanskeIskaznice, idTrening)
  VALUES (1, 1), (1, 2), (2, 1), (2, 2)`;

const sql_insert_upisuje = `INSERT INTO upisuje (brClanskeIskaznice, idStil, idMjesec)
  VALUES (1, 1, 202103), (1, 2, 202103), (1, 1, 202104),(1,2,202104),(1,3,202104), (1,4,202104), (1, 6, 202105), (1, 7, 202105),
  (2, 1, 202103), (2, 2, 202103), (2, 1, 202104), (2, 2, 202104), (2, 6, 202104), (2, 2, 202105), (2, 6, 202105)`;

let table_names = [
  "f",
  "clanarina",
  "uloga",
  "korisnik",
  "clanKluba",
  "vlasnikKluba",
  "trener",
  "drzava",
  "dvorana",
  "mjesec",
  "mjesto",
  "natjecanje",
  "placa",
  "plesniStil",
  "se_natjecao_na",
  "trening",
  "sudjeluje_na",
  "upisuje",
];

let tables = [
  sql_create_f,
  sql_create_clanarina,
  sql_create_uloga,
  sql_create_users,
  sql_create_clanKluba,
  sql_create_vlasnikKluba,
  sql_create_trener,
  sql_create_drzava,
  sql_create_dvorana,
  sql_create_mjesec,
  sql_create_mjesto,
  sql_create_natjecanje,
  sql_create_placa,
  sql_create_plesniStil,
  sql_create_se_natjecao_na,
  sql_create_trening,
  sql_create_sudjeluje_na,
  sql_create_upisuje,
];

let table_data = [
  sql_insert_f,
  sql_insert_clanarina,
  sql_insert_uloga,
  sql_insert_users,
  sql_insert_clanKluba,
  sql_insert_vlasnikKluba,
  sql_insert_trener,
  sql_insert_drzava,
  sql_insert_dvorana,
  sql_insert_mjesec,
  sql_insert_mjesto,
  sql_insert_natjecanje,
  sql_insert_placa,
  sql_insert_plesniStil,
  sql_insert_se_natjecao_na,
  sql_insert_trening,
  sql_insert_sudjeluje_na,
  sql_insert_upisuje,
];

let indexes = [
  "",
  sql_create_clanarina_id_index,
  sql_create_uloga_id_index,
  sql_create_users_id_index,
  sql_create_clanKluba_id_index,
  sql_create_vlasnikKluba_id_index,
  sql_create_trener_id_index,
  sql_create_drzava_id_index,
  sql_create_dvorana_index,
  sql_create_mjesec_index,
  sql_create_mjesto_index,
  sql_create_natjecanje_index,
  sql_create_plesniStil_index,
  sql_create_trening_id_index,
];

let roles = [
  "CREATE ROLE club_owner",
  "CREATE ROLE coach",
  "CREATE ROLE club_member",
  "CREATE ROLE anonymous",
];

let grant_roles = [
  "grant update, delete, insert on table plesnistil to club_owner",
  "grant update, delete, insert on table clanarina to club_owner",
  "grant update, delete, insert on table trener to club_owner",
  "grant update, delete, insert on table dvorana to club_owner",
  "grant select, update, delete, insert on table trening to club_owner, coach",
  "grant select, update, delete, insert on table natjecanje to club_owner, coach",
  "grant select, update, delete, insert on table clankluba to club_owner, coach",
  "grant select on table dvorana to club_owner, club_member, coach, anonymous",
  "grant select on table danceclub.public.mjesto to club_owner, club_member, coach, anonymous",
  "grant select on table plesnistil to club_owner, club_member, coach, anonymous",
  "grant select on table natjecanje to club_owner, club_member, coach, anonymous",
  "grant select on table clanarina to club_owner, club_member, coach, anonymous",
];

let trigger = `CREATE FUNCTION updatetableplaca() RETURNS TRIGGER
LANGUAGE plpgsql
AS
$$
BEGIN
IF tg_op = 'INSERT' THEN
UPDATE placa SET idClanarina = idClanarina + 1 WHERE brClanskeIskaznice = new.brClanskeIskaznice
                        AND idMjesec = new.idMjesec;
IF NOT FOUND THEN 
INSERT INTO placa (brClanskeIskaznice, idMjesec, idClanarina) VALUES
((SELECT new.brClanskeIskaznice), (SELECT new.idMjesec), (SELECT COUNT(idStil) FROM upisuje 													   
 WHERE upisuje.brClanskeIskaznice = new.brClanskeIskaznice AND upisuje.idMjesec = new.idMjesec
 GROUP BY brClanskeIskaznice, idMjesec
));
END IF;
RETURN NEW;
END IF;
END
$$;
CREATE TRIGGER updatePlacaTrigg AFTER INSERT ON upisuje
FOR EACH ROW EXECUTE FUNCTION updatetableplaca();`;

if (tables.length != table_data.length || tables.length != table_names.length) {
  console.log("tables, names and data arrays length mismatch.");
  return;
}

//create tables and populate with data (if provided)

(async () => {
  console.log("Creating and populating tables");
  for (let i = 0; i < tables.length; i++) {
    console.log("Creating table " + table_names[i] + ".");
    try {
      await pool.query(tables[i], []);
      console.log("Table " + table_names[i] + " created.");
      if (table_data[i] !== undefined) {
        try {
          await pool.query(table_data[i], []);
          console.log("Table " + table_names[i] + " populated with data.");
        } catch (err) {
          console.log(
            "Error populating table " + table_names[i] + " with data."
          );
          return console.log(err.message);
        }
      }
    } catch (err) {
      console.log("Error creating table " + table_names[i]);
      return console.log(err.message);
    }
  }

  console.log("Creating indexes");
  for (let i = 0; i < indexes.length; i++) {
    try {
      await pool.query(indexes[i], []);
      console.log("Index " + i + " created.");
    } catch (err) {
      console.log("Error creating index " + i + ".");
    }
  }

  console.log("Creating roles:");
  for (let i = 0; i < roles.length; i++) {
    try {
      await pool.query(roles[i], []);
      console.log("Role " + i + " created.");
    } catch (err) {
      console.log("Error creating role " + i + ".");
    }
  }
  console.log("Granting roles:");
  for (let i = 0; i < grant_roles.length; i++) {
    try {
      await pool.query(grant_roles[i], []);
      console.log("Grant : " + i + " created.");
    } catch (err) {
      console.log("Error creating grant " + i + ".");
    }
  }

  console.log("Creating trigger:");
  try {
    await pool.query(trigger, []);
    console.log("Trigger : created.");
  } catch (err) {
    console.log("Error creating trigger.");
  }
})();
